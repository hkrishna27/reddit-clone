# reddit-clone

### Creating a production ready (spring boot + angular ) SPA app deployed on google k8s engine.

#### Changes to be made:

**Spring Boot** :

    1. apply maven plugins for openapi typescript generator.
    2. script to create front end bundles
    3. swagger documentation
    4. copy resources from web to backend static/
    5. write config to handle cors and serving static resources in k8s cluster

**Angular** :
    
    1. write interceptor to split to remove dns(ex - localhost:8080) so that server can embed their dns
    2. write configuration for production and development(angular.json config and package.json scripts)

**Gitlab** :

    1. copy resources after building maven project to gitlab artifect
    2. define stages ex. build, publish,deploy
    3. use a Dockerfile to copy resources on files(docker-container) to run app in a container.

example: 
```
COPY ${JAR_FILE} reddit-clone-service-0.0.1-SNAPSHOT.jar
COPY --chmod=744 --chown=appuser:appuser reddit-clone-service/target/static/ static/
```

**GKE** :

    1. create a cluster (use command to create cluster)


        a.  Download the existing service account key file (JSON) if you haven’t already.
            You can do this from the -- 
            Google Cloud Console → IAM & Admin → Service Accounts → Your Service Account → Keys → Add Key → Create New Key and select JSON.

            // use this json in your project CI/CD
        
        b.  gcloud container clusters create reddit-clone-cluster     --num-nodes=2 --disk-size=50     --machine-type=e2-medium     --region us-central1

        c.  gcloud container clusters get-credentials reddit-clone-cluster --region us-central1 // this may give error of aceess to cluster so run below command if getting error

        d.  gcloud projects add-iam-policy-binding reddit-clone-431313 \
            --member="serviceAccount:gitlab-c58845e94da04906677bb1@reddit-clone-431313.iam.gserviceaccount.com" \
            --role="roles/container.admin"

        e. kubectl get svc reddit-clone-svc // servc info if services in pending ex-ip run below commands

        f. kubectl describe service reddit-clone-svc

                Name:                     reddit-clone-svc
                Namespace:                default
                Labels:                   app=reddit-clone-svc
                Annotations:              cloud.google.com/neg: {"ingress":true}
                kubernetes.io/change-cause: Force update on $(date +%s)
                Selector:                 run=reddit-clone
                Type:                     LoadBalancer
                IP Family Policy:         SingleStack
                IP Families:              IPv4
                IP:                       34.118.238.175
                IPs:                      34.118.238.175
                Desired LoadBalancer IP:  34.44.242.111
                Port:                     reddit-clone-svc  80/TCP
                TargetPort:               8080/TCP
                NodePort:                 reddit-clone-svc  30974/TCP
                Endpoints:                10.120.5.5:8080
                Session Affinity:         None
                External Traffic Policy:  Cluster
                Internal Traffic Policy:  Cluster
                Events:
                Type     Reason                  Age                  From                Message
                  ----     ------                  ----                 ----                -------
                Normal   EnsuringLoadBalancer    3m12s (x9 over 18m)  service-controller  Ensuring load balancer
                Warning  SyncLoadBalancerFailed  3m11s (x9 over 18m)  service-controller  Error syncing load balancer: failed to ensure load balancer: requested ip "34.44.242.111" is neither static nor assigned to the LB
                g180231018@cloudshell:~ (reddit-clone-431313)$ gcloud compute addresses create reddit-clone-ip --region uscentral1
                ERROR: (gcloud.compute.addresses.create) Could not fetch resource:
                - Permission denied on 'locations/uscentral1' (or it may not exist).
        
        g. gcloud compute addresses create reddit-clone-ip --region us-central1

        h. gcloud compute addresses describe reddit-clone-ip --region us-central1
        
        Created [https://www.googleapis.com/compute/v1/projects/reddit-clone-431313/regions/us-central1/addresses/reddit-clone-ip].
        g180231018@cloudshell:~ (reddit-clone-431313)$ gcloud compute addresses describe reddit-clone-ip --region us-central1
        address: 34.58.75.85
        addressType: EXTERNAL
        creationTimestamp: '2025-02-01T09:15:42.568-08:00'
        description: ''
        id: '6837477394470922321'
        kind: compute#address
        labelFingerprint: 42WmSpB8rSM=
        name: reddit-clone-ip
        networkTier: PREMIUM
        region: https://www.googleapis.com/compute/v1/projects/reddit-clone-431313/regions/us-central1

    2. create a db instance - allow all public ip's (0.0.0.0/0)
    3. now while deploying use k8s deployment and service file to deploy.
    


