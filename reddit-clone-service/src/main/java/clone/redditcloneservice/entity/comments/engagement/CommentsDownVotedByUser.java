package clone.redditcloneservice.entity.comments.engagement;

import clone.redditcloneservice.entity.base.BaseEntity;
import clone.redditcloneservice.entity.comments.CommentsEntity;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "down_voted_comments",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"comment_id", "user_id"})
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentsDownVotedByUser extends BaseEntity {
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @ManyToOne
    @JoinColumn(name = "comment_id")
    private CommentsEntity commentsEntity;
}
