package clone.redditcloneservice.entity.comments.engagement;

import clone.redditcloneservice.entity.base.BaseEntity;
import clone.redditcloneservice.entity.comments.CommentsEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "upvoted_comments",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"comment_id", "user_id"})
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentUpvotedByUser extends BaseEntity {
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @ManyToOne
    @JoinColumn(name = "comment_id")
    private CommentsEntity commentsEntity;
}
