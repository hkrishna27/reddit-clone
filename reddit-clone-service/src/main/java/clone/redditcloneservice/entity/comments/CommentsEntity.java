package clone.redditcloneservice.entity.comments;

import clone.redditcloneservice.entity.base.BaseEntity;
import clone.redditcloneservice.entity.comments.engagement.CommentUpvotedByUser;
import clone.redditcloneservice.entity.comments.engagement.CommentsDownVotedByUser;
import clone.redditcloneservice.entity.post.PostEntity;
import clone.redditcloneservice.entity.security.UserEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(
        name = "reddit_comments",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"id", "user_id"})
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentsEntity extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "parent_id")
    private CommentsEntity parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CommentsEntity> replies = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "post_id", nullable = false)
    private PostEntity post;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @Column(name = "content", nullable = false)
    private String content;

    @OneToMany(mappedBy = "commentsEntity")
    private List<CommentUpvotedByUser> upVotedByUsers =  new ArrayList<>();

    @OneToMany(mappedBy = "commentsEntity")
    private List<CommentsDownVotedByUser> downVotedByUsers =  new ArrayList<>();
}
