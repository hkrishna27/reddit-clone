package clone.redditcloneservice.entity.topics;

import clone.redditcloneservice.entity.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "topic")
public class TopicEntity extends BaseEntity {

    @Column(name = "primary_topic_name")
    private String primaryTopicName;

    @Column(name = "secondary_topic_names")
    private List<String> secondaryTopicNames;
}
