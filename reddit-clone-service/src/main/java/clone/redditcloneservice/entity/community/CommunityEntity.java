package clone.redditcloneservice.entity.community;

import clone.redditcloneservice.entity.security.UserEntity;
import clone.redditcloneservice.enums.AccessibitityEnum;
import clone.redditcloneservice.entity.base.BaseEntity;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "create_community_table")
public class CommunityEntity extends BaseEntity {
    @Column(name = "community_name")
    private String communityName;

    @Column(name = "column_desc" , columnDefinition = "TEXT")
    private String communityDescription;

    @Column(name = "community_profile_image_url")
    private String communityProfileImageUrl;

    @Column(name = "accessibility_type")
    @Enumerated(EnumType.STRING)
    private AccessibitityEnum accessibility;

    @Column(name = "is_adult")
    private boolean isAdult;

    @OneToMany(mappedBy = "communityEntity" , fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    private List<CommunityTopicEntity> topicEntities;

    @ManyToMany(mappedBy = "communities" , fetch = FetchType.LAZY)
    private Set<UserEntity> communityMembers = new HashSet<>();
}
