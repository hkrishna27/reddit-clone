package clone.redditcloneservice.entity.community;

import clone.redditcloneservice.entity.base.BaseEntity;
import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "community_topic")
public class CommunityTopicEntity extends BaseEntity {
    @Column(name = "topic_name")
    private String topicName;

    @ManyToOne
    @JoinColumn(name = "community_id")
    private CommunityEntity communityEntity;
}
