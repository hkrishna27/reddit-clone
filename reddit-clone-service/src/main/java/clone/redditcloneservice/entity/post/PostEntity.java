package clone.redditcloneservice.entity.post;

import clone.redditcloneservice.entity.base.BaseEntity;
import clone.redditcloneservice.entity.security.UserEntity;
import clone.redditcloneservice.enums.PostTagsEnum;
import clone.redditcloneservice.enums.PostTypeEnum;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "posts")
public class PostEntity extends BaseEntity {
    @Column(name = "community_id" , updatable = false)
    private Long communityId;

    @Enumerated(EnumType.STRING)
    @Column(name = "post_type")
    private PostTypeEnum postType;

    @Enumerated(EnumType.STRING)
    @Column(name = "post_tags")
    private List<PostTagsEnum> postTags;

    @Column(name = "text_title")
    private String textTitle;

    @Column(name = "text_body", columnDefinition = "TEXT")
    private String textBody;

    @OneToMany(mappedBy = "postRequestEntity" , cascade = CascadeType.ALL)
    private List<ImageUrlEntity> imageUrls;

    @OneToMany(mappedBy = "postRequestEntity" , cascade = CascadeType.ALL)
    private List<VideoUrlEntity> videoUrls;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @Column(name = "external_link")
    private String link;

}
