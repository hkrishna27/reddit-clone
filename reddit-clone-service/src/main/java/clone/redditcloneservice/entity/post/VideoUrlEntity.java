package clone.redditcloneservice.entity.post;

import clone.redditcloneservice.entity.base.BaseEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "video_urls")
public class VideoUrlEntity extends BaseEntity {
    @Column(name = "url_path")
    private String urlPath;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private PostEntity postRequestEntity;
}
