package clone.redditcloneservice.entity.post.engagement;

import clone.redditcloneservice.entity.base.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.*;

@Entity
@Table(
        name = "post_engagement",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"community_id", "post_id", "user_id"})
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostEngagementEntity extends BaseEntity {
    @Column(name = "community_id", nullable = false)
    private Long communityId;

    @Column(name = "post_id", nullable = false)
    private Long postId;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "up_voted")
    private boolean upVoted;

    @Column(name = "down_voted")
    private boolean downVoted;
}
