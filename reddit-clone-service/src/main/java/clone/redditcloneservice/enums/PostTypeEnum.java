package clone.redditcloneservice.enums;

public enum PostTypeEnum {
    TEXT, IMAGE_AND_VIDEO , LINK , POLL;
}
