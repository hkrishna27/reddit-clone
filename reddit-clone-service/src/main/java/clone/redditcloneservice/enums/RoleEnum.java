package clone.redditcloneservice.enums;

public enum RoleEnum {
        USER,
        MODERATOR,
        ADMIN
}
