package clone.redditcloneservice.config.jwt;

import clone.redditcloneservice.utils.UserServiceImpl;
import clone.redditcloneservice.utils.JwtUtilsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Configuration
public class JwtAuthFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtilsService jwtUtilsService;
    @Autowired
    private UserServiceImpl userDetailsService;

    private final ObjectMapper objectMapper= new ObjectMapper();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
            final String authHeader = request.getHeader("Authorization");
            final String jwt;
            final String userName;
            // checking if headers not present
            if (StringUtils.isEmpty(authHeader) || !StringUtils.startsWith(authHeader, "Bearer")) {
                filterChain.doFilter(request, response);
                return;
            }
            // fetching token
            jwt = authHeader.substring(7);
            try {
                userName = jwtUtilsService.extractUserName(jwt);
                // if everything good set authentication
                if (StringUtils.isNotEmpty(userName)
                        && SecurityContextHolder.getContext().getAuthentication() == null) {
                    UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
                    if (jwtUtilsService.isTokenValid(jwt,userDetails)) {
                        SecurityContext context = SecurityContextHolder.createEmptyContext();
                        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                                userDetails, null, userDetails.getAuthorities());
                        authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        context.setAuthentication(authToken);
                        SecurityContextHolder.setContext(context);
                    }
                }
            } catch(ExpiredJwtException e) {
                // handling this via an error controller because filter can't directly call controller advice
                // therefore dispatching a request explicitly and then handled by global exception handler
                request.getRequestDispatcher("/api/auth/error/expired-jwt").forward(request, response);
            } catch(SignatureException e) {
                // handling this via an error controller because filter can't directly call controller advice
                // therefore dispatching a request explicitly and then handled by global exception handler
                request.getRequestDispatcher("/api/auth/error/mismatched-jwt-signature").forward(request, response);
                return;
            }
        filterChain.doFilter(request, response);
    }
}
