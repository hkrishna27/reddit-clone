package clone.redditcloneservice.controller;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.enums.CommunityEnum;
import clone.redditcloneservice.model.community.CommunityRequest;
import clone.redditcloneservice.model.community.CommunityResponse;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.service.CommunityService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(GlobalConstants.API+ UrlConstants.COMMUNITY_URI)
public class CommunityController {

    private final CommunityService communityService;

    @Autowired
    public CommunityController(CommunityService communityService) {
        this.communityService = communityService;
    }

    @PostMapping(value = "/create", consumes = "application/json", produces = "application/json")
    public ResponseEntity<MessageResponse> createCommunity(@Valid @RequestBody CommunityRequest communityRequest) {
        return communityService.createCommunity(communityRequest);
    }

    @GetMapping(value = "/fetchAll", produces = "application/json")
    public ResponseEntity<List<CommunityResponse>> fetchAllCommunities() {
        return communityService.fetchAllCommunities();
    }

    @GetMapping(value = "/existsByCommunityName", produces = "application/json")
    public ResponseEntity<Boolean> existsByCommunityName(@RequestParam String communityName) {
        return communityService.existsByCommunityName(communityName);
    }

    @GetMapping(value = "/{communityId}", produces = "application/json")
    public ResponseEntity<CommunityResponse> fetchCommunityById(@PathVariable Long communityId) {
        return communityService.fetchCommunityById(communityId);
    }

    @PostMapping(value = "/join")
    public ResponseEntity<MessageResponse> joinCommunity(@RequestParam Long userId, @RequestParam Long communityId, @RequestParam CommunityEnum operation) {
        return communityService.joinCommunity(userId,communityId,operation);
    }
}
