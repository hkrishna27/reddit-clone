package clone.redditcloneservice.controller.comments;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.model.comment.CommentRequest;
import clone.redditcloneservice.model.comment.CommentResponse;
import clone.redditcloneservice.model.generic_response.DataResponse;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.model.page.PageableRequest;
import clone.redditcloneservice.model.post.PostRequest;
import clone.redditcloneservice.model.post.PostResponse;
import clone.redditcloneservice.service.comments.CommentService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(GlobalConstants.API + UrlConstants.COMMENT_URI)
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }
    @PostMapping(value = "/add", produces = "application/json" , consumes = "application/json")
    public ResponseEntity<DataResponse<MessageResponse>> addComment(@Valid @RequestBody CommentRequest commentRequest) {
        return ResponseEntity.ok(commentService.addComment(commentRequest));
    }

    @GetMapping(value = "/get/{postId}", produces = "application/json")
    public ResponseEntity<Page<CommentResponse>> getCommentsByPostId(@PathVariable Long postId, @ModelAttribute PageableRequest pageableRequest) {
        return ResponseEntity.ok(commentService.getCommentsByPostId(postId, pageableRequest));
    }
}
