package clone.redditcloneservice.controller.comments;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.model.comment.engagement.CommentEngagement;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.model.post.engagement.PostEngagement;
import clone.redditcloneservice.service.comments.CommentEngagementService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(GlobalConstants.API + UrlConstants.POST_ENGAGEMENT_URI)
public class CommentsEngagementController {
    private final CommentEngagementService commentEngagementService;

    @Autowired
    public CommentsEngagementController(CommentEngagementService commentEngagementService) {
        this.commentEngagementService = commentEngagementService;
    }

    @PostMapping(value = "/performOps", consumes = "application/json", produces = "application/json")
    public ResponseEntity<MessageResponse> performEngagementOpsOnComments(@RequestBody @Valid CommentEngagement commentEngagement) {
        return this.commentEngagementService.performEngagementOpsOnComments(commentEngagement);
    }
}
