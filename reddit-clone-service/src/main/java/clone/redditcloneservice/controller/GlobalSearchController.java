package clone.redditcloneservice.controller;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.model.search.NonEnrichedSearchResponse;
import clone.redditcloneservice.service.GlobalSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(GlobalConstants.API + UrlConstants.GLOBAL_SEARCH)
public class GlobalSearchController {

    private final GlobalSearchService globalSearchService;

    @Autowired
    public GlobalSearchController(GlobalSearchService globalSearchService) {
        this.globalSearchService = globalSearchService;
    }

    @GetMapping(value = "/non-enriched", produces = "application/json")
    public ResponseEntity<NonEnrichedSearchResponse> getNonEnrichedSearchResults(@RequestParam String searchText) {
        return globalSearchService.getNonEnrichedSearchResults(searchText);
    }
}
