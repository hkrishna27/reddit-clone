package clone.redditcloneservice.controller;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.model.topics.TopicRequest;
import clone.redditcloneservice.model.topics.Topics;
import clone.redditcloneservice.model.generic_response.DataResponse;
import clone.redditcloneservice.service.TopicService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(GlobalConstants.API + UrlConstants.TOPIC_URI)
public class TopicController {

    private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping(value="/get-topics", produces = "application/json")
    public ResponseEntity<DataResponse<List<Topics>>> getAllTopics() {
        return topicService.getAllTopics();
    }

    @PostMapping(value="/create-topics", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> createTopics(@RequestBody TopicRequest topicRequest) {
        return topicService.createTopic(topicRequest);
    }
    @PutMapping(value="/update-topics", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> updateTopics() {
        return null;
    }
    @DeleteMapping(value="/delete-topics", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> deleteTopic() {
        return null;
    }
}
