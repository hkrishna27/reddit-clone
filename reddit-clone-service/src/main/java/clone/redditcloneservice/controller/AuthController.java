package clone.redditcloneservice.controller;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.enums.RoleEnum;
import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.model.auth.LoginRequest;
import clone.redditcloneservice.model.auth.SignupRequest;
import clone.redditcloneservice.model.auth.UserResponse;
import clone.redditcloneservice.service.AuthService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Objects;


@RestController
@RequestMapping(GlobalConstants.API+ UrlConstants.AUTH_URI)
public class AuthController {
    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }


    @PostMapping(value = "/signin",consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest request) {
        return this.authService.authenticate(request);
    }

    @PostMapping(value = "/signup" , consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest request) {
        if(Objects.isNull(request.getRoles()) || request.getRoles().isEmpty()) {
            request.setRoles(Collections.singleton(RoleEnum.USER));
        }
        return this.authService.signUpUser(request);
    }

    @PostMapping("/role")
    public ResponseEntity<?> saveRoles() {
        return this.authService.saveRolesToSystem();
    }

}
