package clone.redditcloneservice.controller;

import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.constant.ValidationConstants;
import clone.redditcloneservice.model.s3.SignedUrlRequestS3;
import clone.redditcloneservice.model.s3.SignedUrlResponseS3;
import clone.redditcloneservice.service.S3Service;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(UrlConstants.S3_URI)
public class S3Controller {
    private final S3Service s3Service;

    public S3Controller(S3Service s3Service) {
        this.s3Service = s3Service;
    }

    @PostMapping(
            value = "/get-signed-url",
            produces =  { MediaType.APPLICATION_JSON_VALUE },
            consumes =  { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<SignedUrlResponseS3> getSignedUrlS3(@RequestBody SignedUrlRequestS3 request) {
        return ResponseEntity.ok(s3Service.generateSignedUrlS3(request));
    }

    @PostMapping(
            value = "/get-signed-urls",
            produces =  { MediaType.APPLICATION_JSON_VALUE },
            consumes =  { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<List<SignedUrlResponseS3>> getSignedUrlsS3(@RequestBody List<SignedUrlRequestS3> requests) {
        return ResponseEntity.ok(s3Service.generateSignedUrlsS3(requests));
    }

    @PostMapping(path = "/upload", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public String uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        s3Service.uploadFile(file.getOriginalFilename(), file);
        return "File uploaded";
    }
}
