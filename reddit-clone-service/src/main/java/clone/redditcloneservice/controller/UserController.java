package clone.redditcloneservice.controller;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.model.auth.UserResponse;
import clone.redditcloneservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(GlobalConstants.API + UrlConstants.USER)
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
}
