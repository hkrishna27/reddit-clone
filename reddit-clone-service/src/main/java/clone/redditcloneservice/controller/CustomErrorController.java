package clone.redditcloneservice.controller;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.constant.UrlConstants;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * Here we are handling the errors thrown by our jwt auth filter
 */
@RestController
@RequestMapping(GlobalConstants.API + UrlConstants.CUSTOM_ERROR_URI)
public class CustomErrorController {
    @RequestMapping(value = "/expired-jwt", produces = "application/json",  method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.PATCH})
    public ResponseEntity<?> throwExpiredJwtException(HttpServletRequest request)  {
        throw new ExpiredJwtException(null,null, "token expired.");
    }

    @RequestMapping(value = "/mismatched-jwt-signature", produces = "application/json", method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.PATCH})
    public ResponseEntity<?> throwMismatchedJwtSignatureException(HttpServletRequest request) throws SignatureException {
        throw new SignatureException("JWT signature does not match locally computed signature.");
    }
}
