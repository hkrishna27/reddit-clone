package clone.redditcloneservice.controller;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.model.post.engagement.PostEngagement;
import clone.redditcloneservice.service.PostEngagementService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(GlobalConstants.API + UrlConstants.POST_URI + UrlConstants.POST_ENGAGEMENT_URI)
public class PostEngagementController {
    private final PostEngagementService postEngagementService;

    @Autowired
    public PostEngagementController(PostEngagementService postEngagementService) {
        this.postEngagementService = postEngagementService;
    }

    @PostMapping(value = "/upvoteOrDownVote", consumes = "application/json", produces = "application/json")
    public ResponseEntity<MessageResponse> upvoteOrDownVotePost(@RequestBody @Valid PostEngagement postEngagement) {
        return this.postEngagementService.upvoteOrDownVotePost(postEngagement);
    }

    @DeleteMapping(value = "/delete/{engagementId}", produces = "application/json")
    public ResponseEntity<MessageResponse> deleteEngagementWithPost(@PathVariable Long engagementId) {
        return this.postEngagementService.deleteEngagementWithPost(engagementId);
    }
}
