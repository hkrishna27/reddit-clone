package clone.redditcloneservice.controller;

import clone.redditcloneservice.constant.GlobalConstants;
import clone.redditcloneservice.constant.UrlConstants;
import clone.redditcloneservice.enums.PostCatagoryEnum;
import clone.redditcloneservice.model.generic_response.DataResponse;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.model.page.PageableRequest;
import clone.redditcloneservice.model.post.PostRequest;
import clone.redditcloneservice.model.post.PostResponse;
import clone.redditcloneservice.service.PostService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(GlobalConstants.API + UrlConstants.POST_URI)
public class PostController {

    private final PostService postService;

    private static final Logger logger = LoggerFactory.getLogger(PostController.class);

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping(value = "/create", produces = "application/json" , consumes = "application/json")
    public ResponseEntity<DataResponse<MessageResponse>> createRedditPost(@Valid @RequestBody PostRequest postRequest) {
        long startTime = System.currentTimeMillis();
        ResponseEntity<DataResponse<MessageResponse>> response = postService.createRedditPost(postRequest);
        long endTime = System.currentTimeMillis();
        logger.info("Total time taken {}", endTime - startTime);
        return response;
    }

    @GetMapping(value = "/get/{communityId}", produces = "application/json")
    public ResponseEntity<Page<PostResponse>> getPostsByCommunityId(@PathVariable Long communityId, @ModelAttribute PageableRequest pageableRequest) {
        return postService.getPostsByCommunityId(communityId, pageableRequest);
    }

    @GetMapping(value = "/get/category", produces = "application/json")
    public ResponseEntity<Page<PostResponse>> getPostsByCategory(
            @RequestParam PostCatagoryEnum postCategory,
            @ModelAttribute PageableRequest pageableRequest) {
        return postService.getPostsByCategory(postCategory, pageableRequest);
    }

    @GetMapping(value = "/get/category/{categoryType}", produces = "application/json")
    public ResponseEntity<Page<PostResponse>> getPostsByCategoryType(
            @PathVariable String categoryType,
            @ModelAttribute PageableRequest pageableRequest) {
        return postService.getPostsByCategoryType(categoryType, pageableRequest);
    }

    @GetMapping(value = "/{postId}", produces = "application/json")
    public ResponseEntity<PostResponse> getPostById(@PathVariable Long postId, @RequestParam Long communityId) {
        return ResponseEntity.ok(postService.findSinglePostData(postId, communityId));
    }
}
