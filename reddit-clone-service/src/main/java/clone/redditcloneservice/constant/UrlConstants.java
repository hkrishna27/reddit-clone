package clone.redditcloneservice.constant;

public final class UrlConstants {
    public static final String POST_URI = "posts";
    public static final String S3_URI = "/api/s3";
    public static final String TOPIC_URI = "topic";
    public static final String CUSTOM_ERROR_URI = "auth/error";
    public static final String COMMUNITY_URI = "community";
    public static final String AUTH_URI = "auth";
    public static final String USER = "user";
    public static final String POST_ENGAGEMENT_URI = "/engagement";
    public static final String COMMENT_URI = "comments";
    public static final String COMMENT_ENGAGEMENT = "engagement";
    public static final String GLOBAL_SEARCH = "search";
}
