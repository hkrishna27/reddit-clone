package clone.redditcloneservice.constant;

public final class ValidationConstants {
    public final static String SUCCESS = "SUCCESS";
    public final static String SOMETHING_WENT_WRONG = "Something went wrong.";
    public final static String OPERATION_ERROR = "Operation can't be performed.";
    public final static String NOT_EMPTY = "can't be empty.";
    public final static String NOT_NULL = "can't be null.";
    public static final String NO_SUCH_USER = "No such user";
    public static final String NO_SUCH_COMMUNITY = "No such community" ;
    public static final String NO_SUCH_COMMUNITY_OR_USER = "community or user do not exist." ;
    public static final String NO_SUCH_POST = "No Such Post" ;
    public static final String PARENT_COMMENT_NOT_FOUND = "Parent Comment Not Found" ;
    public static final String COMMENT_NOT_FOUND = "No comment found" ;
    public static final String GOOGLE_SIGN_IN_FAILED = "Google sign in failed.";
}
