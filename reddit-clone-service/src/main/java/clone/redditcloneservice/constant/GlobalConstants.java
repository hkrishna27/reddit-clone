package clone.redditcloneservice.constant;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public final class GlobalConstants {

    private GlobalConstants() {}
    // auth
    public final static String[] WHITELIST = {"/error","/api/auth/**","/v3/api-docs/**", "/swagger-ui/**"};
    public final static String[] STATIC_RESOURCES = {"/", "/index.html", "/favicon.ico", "/assets/**", "/*.js", "/*.css"};
    public final static String API = "/api/";
    public final static String SORT_BY = "id";
    public final static String SORT_DIRECTION = "ASC";
}
