package clone.redditcloneservice.constant;

public final class CommentsConstants {
    public static final String UP_VOTE = "UpVote";
    public static final String DOWN_VOTE = "DownVote";
}
