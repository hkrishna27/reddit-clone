package clone.redditcloneservice.mapper;

import clone.redditcloneservice.entity.comments.CommentsEntity;
import clone.redditcloneservice.entity.community.CommunityEntity;
import clone.redditcloneservice.entity.community.CommunityTopicEntity;
import clone.redditcloneservice.model.comment.CommentRequest;
import clone.redditcloneservice.model.community.CommunityRequest;
import clone.redditcloneservice.model.community.CommunityResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CommunityMapper {

    CommunityMapper MAPPER = Mappers.getMapper(CommunityMapper.class);

    @Mapping(source = "topics", target = "topicEntities")
    CommunityEntity mapCommunityRequestToEntity(CommunityRequest communityRequest);

    @Mapping(source = "communityEntity", target = "communityEntity")
    CommunityTopicEntity mapCreateCommunityEntityToCommunityTopicEntity(CommunityEntity communityEntity);

    @Mapping(source = "topicEntities", target = "topics")
    CommunityResponse mapCommunityEntityToCommunityResponseModel(CommunityEntity communityEntity);
    List<CommunityResponse> mapCommunityEntityToCommunityResponseModel(List<CommunityEntity> communityEntities);

}
