package clone.redditcloneservice.mapper.comments;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CommentsMapper {
    CommentsMapper COMMENTS_MAPPER = Mappers.getMapper(CommentsMapper.class);

}
