package clone.redditcloneservice.mapper;

import clone.redditcloneservice.entity.security.RoleEntity;
import clone.redditcloneservice.entity.security.UserEntity;
import clone.redditcloneservice.model.auth.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserMapper  MAPPER = Mappers.getMapper(UserMapper.class);

    default UserResponse mapUserEntityToUserResponse(UserEntity userEntity) {
        return UserResponse.builder()
                .userId(userEntity.getId())
                .userName(userEntity.getUsername())
                .userEmail(userEntity.getEmail())
                .userRoles(userEntity.getRoles().stream().map(RoleEntity::getRoleName).toList())
                .build();
    }
}
