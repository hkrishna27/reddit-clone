package clone.redditcloneservice.mapper;

import clone.redditcloneservice.entity.post.engagement.PostEngagementEntity;
import clone.redditcloneservice.model.post.engagement.PostEngagement;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PostEngagementMapper {
    PostEngagementMapper MAPPER = Mappers.getMapper(PostEngagementMapper.class);

    PostEngagementEntity mapPostEngagementToEntity(PostEngagement postEngagement);

    List<PostEngagement> mapEngagementEntityListToEngagementList(List<PostEngagementEntity> engagementList);
}
