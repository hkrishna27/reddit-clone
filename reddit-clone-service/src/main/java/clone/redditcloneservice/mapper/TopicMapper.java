package clone.redditcloneservice.mapper;

import clone.redditcloneservice.entity.topics.TopicEntity;
import clone.redditcloneservice.model.topics.Topics;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TopicMapper {

    TopicMapper MAPPER = Mappers.getMapper(TopicMapper.class);

    TopicEntity topicsToTopicEntity(Topics topics);

    Topics topicEntityToTopics(TopicEntity topicEntity);

    List<Topics> topicEntityListToTopic(List<TopicEntity> topicEntities);
}
