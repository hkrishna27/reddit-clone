package clone.redditcloneservice.mapper;

import clone.redditcloneservice.entity.post.PostEntity;
import clone.redditcloneservice.entity.security.RoleEntity;
import clone.redditcloneservice.entity.security.UserEntity;
import clone.redditcloneservice.mapper.comments.CommentsMapper;
import clone.redditcloneservice.model.auth.UserResponse;
import clone.redditcloneservice.model.post.PostRequest;
import clone.redditcloneservice.model.post.PostResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface PostMapper {

    PostMapper MAPPER = Mappers.getMapper(PostMapper.class);

    @Mapping(source = "postTags", target = "postTags")
    PostEntity postRequestToPostRequestEntity(PostRequest postRequest, @MappingTarget PostEntity postRequestEntity);

    @Mapping(source = "userEntity", target = "userResponse" , qualifiedByName = "mapUserEntityToUserResponse")
    @Mapping(source = "imageUrls", target = "imageUrls")
    @Mapping(source = "videoUrls", target = "videoUrls")
    PostResponse postEntityToPostResponse(PostEntity postRequestEntity);
    List<PostResponse> postEntityListToPostResponseList(List<PostEntity> postRequestEntity);

    @Named("mapUserEntityToUserResponse")
    default UserResponse mapUserEntityToUserResponse(UserEntity userEntity) {
        return UserResponse.builder()
                .userId(userEntity.getId())
                .userName(userEntity.getUsername())
                .userEmail(userEntity.getEmail())
                .userRoles(userEntity.getRoles().stream().map(RoleEntity::getRoleName).toList())
                .build();
    }
}
