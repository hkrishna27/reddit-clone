package clone.redditcloneservice.model.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TokenResponse implements Serializable {
    private String token;
    private String userName;
    private String email;
    private List<String> roles;
}
