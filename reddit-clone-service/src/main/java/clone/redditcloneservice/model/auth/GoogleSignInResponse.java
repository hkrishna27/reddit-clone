package clone.redditcloneservice.model.auth;

import lombok.Data;

import java.util.Map;

@Data
public class GoogleSignInResponse {
        private String id;
        private String name;
        private String email;
        private String photoUrl;
        private String provider;
        private String authToken;
        private String idToken;
        private String authorizationCode;
        private Map<String, Object> response;
}
