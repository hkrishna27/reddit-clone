package clone.redditcloneservice.model.auth;

import clone.redditcloneservice.enums.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserResponse {
    private Long userId;
    private String userName;
    private String userEmail;
    private List<RoleEnum> userRoles;
}
