package clone.redditcloneservice.model.community;

import clone.redditcloneservice.enums.AccessibitityEnum;
import clone.redditcloneservice.constant.ValidationConstants;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Data
@Builder
@Validated // used for class level validation
public class CommunityRequest {
    @NotEmpty(message = ValidationConstants.NOT_EMPTY)
    private String communityName;

    @NotEmpty(message = ValidationConstants.NOT_EMPTY)
    private String communityDescription;

    private String communityProfileImageUrl;

    @NotNull(message = ValidationConstants.NOT_NULL)
    private AccessibitityEnum accessibility;

    private boolean isAdult;

    @Valid // for nested validation --> method or field level
    @NotEmpty(message = ValidationConstants.NOT_EMPTY)
    List<CommunityTopicRequest> topics;
}