package clone.redditcloneservice.model.community;

import clone.redditcloneservice.constant.ValidationConstants;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class CommunityTopicRequest {
    @NotEmpty(message = ValidationConstants.NOT_EMPTY)
    private String topicName;
}
