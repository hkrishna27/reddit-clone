package clone.redditcloneservice.model.community;

import clone.redditcloneservice.entity.security.UserEntity;
import clone.redditcloneservice.enums.AccessibitityEnum;
import clone.redditcloneservice.model.auth.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommunityResponse {
    private Long id;
    private String communityName;
    private String communityDescription;
    private String communityProfileImageUrl;
    private AccessibitityEnum accessibility;
    private boolean isAdult;
    private UserResponse loggedUserMember;
    private Integer totalCommunityMembers;
    private List<UserResponse> communityMembers;
    List<CommunityTopicResponse> topics;
}
