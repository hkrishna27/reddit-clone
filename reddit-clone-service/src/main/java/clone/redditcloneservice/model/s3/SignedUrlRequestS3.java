package clone.redditcloneservice.model.s3;

import com.amazonaws.HttpMethod;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignedUrlRequestS3 {
    private String fileName;
    private String s3BucketFolder;
    private HttpMethod httpMethod;
}
