package clone.redditcloneservice.model.s3;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignedUrlResponseS3 {
    String fileName;
    String signedUrl;
}
