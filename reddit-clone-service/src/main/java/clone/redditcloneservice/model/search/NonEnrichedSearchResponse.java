package clone.redditcloneservice.model.search;

import clone.redditcloneservice.model.comment.CommentResponse;
import clone.redditcloneservice.model.community.CommunityResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NonEnrichedSearchResponse {
    private List<CommunityResponse> communities;
}
