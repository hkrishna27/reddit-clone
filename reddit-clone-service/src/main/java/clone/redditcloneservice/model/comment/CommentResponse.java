package clone.redditcloneservice.model.comment;

import clone.redditcloneservice.model.auth.UserResponse;
import clone.redditcloneservice.model.post.PostResponse;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommentResponse {
    private Long id;
    private Long parentCommentId;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String content;
    private UserResponse userResponse;
    private Long postResponseId;
    private Long totalUpvoteCount;
    private Long totalDownVoteCount;
    private Boolean upvotedByCurrentUser;
    private Boolean downVotedByCurrentUser;
    private List<CommentResponse> replies;
}
