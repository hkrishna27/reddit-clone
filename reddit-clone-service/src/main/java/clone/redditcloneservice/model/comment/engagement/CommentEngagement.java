package clone.redditcloneservice.model.comment.engagement;

import clone.redditcloneservice.constant.ValidationConstants;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentEngagement {
    private Long commentId;
    private boolean upVoted;
    private boolean downVoted;
    private boolean addNewEngagement;
    private boolean removeExistingEngagement;
    private boolean switchExistingEngagement;
}
