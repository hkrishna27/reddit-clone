package clone.redditcloneservice.model.comment;

import clone.redditcloneservice.constant.ValidationConstants;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommentRequest {
    private Long id;

    private Long parentCommentId;

    @NotNull(message = ValidationConstants.NOT_NULL)
    private Long postId;

    @NotNull(message = ValidationConstants.NOT_NULL)
    private Long userId;

    @NotEmpty(message = ValidationConstants.NOT_EMPTY)
    private String content;
}
