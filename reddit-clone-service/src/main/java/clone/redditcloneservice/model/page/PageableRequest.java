package clone.redditcloneservice.model.page;

import clone.redditcloneservice.constant.GlobalConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageableRequest {
    private int page = 0;
    private int size = 10;
    private String sortBy = GlobalConstants.SORT_BY;
    private String direction = GlobalConstants.SORT_DIRECTION;

    public Pageable toPageable() {
        return PageRequest.of(
                page,
                size,
                Sort.by(Sort.Direction.fromString(direction), sortBy)
        );
    }
}

