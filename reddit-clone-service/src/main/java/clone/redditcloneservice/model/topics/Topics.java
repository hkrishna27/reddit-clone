package clone.redditcloneservice.model.topics;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Topics {
    @NotNull
    private String primaryTopicName;
    @NotEmpty
    private List<String> secondaryTopicNames;
}
