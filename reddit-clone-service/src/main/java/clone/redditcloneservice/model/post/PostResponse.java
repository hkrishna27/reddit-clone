package clone.redditcloneservice.model.post;

import clone.redditcloneservice.entity.post.ImageUrlEntity;
import clone.redditcloneservice.entity.post.VideoUrlEntity;
import clone.redditcloneservice.enums.PostTagsEnum;
import clone.redditcloneservice.enums.PostTypeEnum;
import clone.redditcloneservice.model.auth.UserResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostResponse {
    private Long id;
    private String createdBy;
    private LocalDateTime updatedAt;
    private Long communityId;
    private PostTypeEnum postType;
    private List<PostTagsEnum> postTags;
    private String textTitle;
    private String textBody;
    private List<UrlRequest> imageUrls;
    private List<UrlRequest> videoUrls;
    private String link;
    private UserResponse userResponse;
    private Long upVotesCount = 0L;
    private Long downVotesCount = 0L;
    private Long currentUserPostEngagementId;
    private boolean currentUserUpVoted;
    private boolean currentUserDownVoted;
    private Long commentsCount;
}
