package clone.redditcloneservice.model.post.engagement;

import clone.redditcloneservice.constant.ValidationConstants;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostEngagement {
    private Long id;
    @NotNull(message = ValidationConstants.NOT_NULL)
    private Long communityId;
    @NotNull(message = ValidationConstants.NOT_NULL)
    private Long postId;
    private boolean upVoted;
    private boolean downVoted;
}
