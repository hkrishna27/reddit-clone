package clone.redditcloneservice.model.post;

import clone.redditcloneservice.constant.ValidationConstants;
import clone.redditcloneservice.enums.PostTagsEnum;
import clone.redditcloneservice.enums.PostTypeEnum;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostRequest {
    @NotNull(message = ValidationConstants.NOT_NULL)
    private Long communityId;

    @NotNull(message = ValidationConstants.NOT_NULL)
    private PostTypeEnum postType;

    private List<PostTagsEnum> postTags;

    @NotEmpty(message = ValidationConstants.NOT_EMPTY)
    private String textTitle;

    private String textBody;

    private List<UrlRequest> imageUrls;

    private List<UrlRequest> videoUrls;

    private String link;

    @NotNull(message = ValidationConstants.NOT_NULL)
    private Long userId;
}
