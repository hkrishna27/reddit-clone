package clone.redditcloneservice.service;

import clone.redditcloneservice.constant.ValidationConstants;
import clone.redditcloneservice.entity.post.engagement.PostEngagementEntity;
import clone.redditcloneservice.mapper.PostEngagementMapper;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.model.post.engagement.PostEngagement;
import clone.redditcloneservice.repository.PostEngagementRepository;
import clone.redditcloneservice.utils.UserDetailsImpl;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Tuple;
import jakarta.persistence.criteria.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostEngagementService {
    private final PostEngagementRepository postEngagementRepository;

    @PersistenceContext
    private final EntityManager entityManager;

    @Autowired
    public PostEngagementService(PostEngagementRepository postEngagementRepository, EntityManager entityManager) {
        this.postEngagementRepository = postEngagementRepository;
        this.entityManager = entityManager;
    }

    public ResponseEntity<MessageResponse> upvoteOrDownVotePost(PostEngagement postEngagement) {
        PostEngagementEntity postEngagementEntity = PostEngagementMapper.MAPPER.mapPostEngagementToEntity(postEngagement);
        // current user
        UserDetailsImpl principal = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        postEngagementEntity.setUserId(principal.getId());
        PostEngagementEntity save = postEngagementRepository.save(postEngagementEntity);
        return ResponseEntity.ok(new MessageResponse(ValidationConstants.SUCCESS, save.getId()));
    }

    public ResponseEntity<MessageResponse> deleteEngagementWithPost(Long id) {
        postEngagementRepository.deleteById(id);
        return ResponseEntity.ok(new MessageResponse(ValidationConstants.SUCCESS, null));
    }

    public List<Tuple> getTotalUpVotesAndDownVotes(List<Long> postIds, Long communityId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> query = cb.createQuery(Tuple.class);
        Root<PostEngagementEntity> root = query.from(PostEngagementEntity.class);
        // Create the predicates
        Predicate postIdPredicate = root.get("postId").in(postIds);

        // Only add the communityId predicate if communityId is not null
        Predicate communityIdPredicate = communityId != null ?
                cb.equal(root.get("communityId"), communityId) : cb.conjunction();  // cb.conjunction() means "always true"

        // Conditional counts
        Expression<Long> upVoteCount = cb.sum(cb.<Long>selectCase()
                .when(cb.isTrue(root.get("upVoted")), 1L)
                .otherwise(0L));
        Expression<Long> downVoteCount = cb.sum(cb.<Long>selectCase()
                .when(cb.isTrue(root.get("downVoted")), 1L)
                .otherwise(0L));

        // Build the query
        query.multiselect(
                        root.get("postId"),
                        upVoteCount.alias("upVoteCount"),
                        downVoteCount.alias("downVoteCount")
                )
                .where(cb.and(postIdPredicate, communityIdPredicate))
                .groupBy(root.get("postId"));

        // Execute the query
        return entityManager.createQuery(query).getResultList();
    }


    List<PostEngagement> fetchEngagementDataForPostWithUser(List<Long> postIds, Long communityId, Long userId) {
        // create criteria builder
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        // create criteria query
        CriteriaQuery<PostEngagementEntity> query = cb.createQuery(PostEngagementEntity.class);
        // define root
        Root<PostEngagementEntity> root = query.from(PostEngagementEntity.class);
        // define all predicates
        Predicate findPostsWithPostIds = root.get("postId").in(postIds);
        Predicate postWithCommunityId = communityId != null ?
                cb.equal(root.get("communityId"), communityId) : cb.conjunction();
        Predicate postWithUserId = cb.equal(root.get("userId"), userId);
        // write select query
        query.select(root).where(cb.and(findPostsWithPostIds,postWithCommunityId,postWithUserId));
        // create query
        List<PostEngagementEntity> engagementList = entityManager.createQuery(query).getResultList();
        return PostEngagementMapper.MAPPER.mapEngagementEntityListToEngagementList(engagementList);
    }
}
