package clone.redditcloneservice.service;

import clone.redditcloneservice.model.search.NonEnrichedSearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class GlobalSearchService {

    private final CommunityService communityService;

    @Autowired
    public GlobalSearchService(CommunityService communityService) {
        this.communityService = communityService;
    }

    public ResponseEntity<NonEnrichedSearchResponse> getNonEnrichedSearchResults(String searchText) {
        NonEnrichedSearchResponse nonEnrichedSearchResponse = new NonEnrichedSearchResponse();
        nonEnrichedSearchResponse.setCommunities(communityService.findCommunitiesMatchesLike(searchText,10));
        return ResponseEntity.ok(nonEnrichedSearchResponse);
    }
}
