package clone.redditcloneservice.service;

import clone.redditcloneservice.constant.ValidationConstants;
import clone.redditcloneservice.entity.topics.TopicEntity;
import clone.redditcloneservice.exception.CustomException;
import clone.redditcloneservice.mapper.TopicMapper;
import clone.redditcloneservice.model.topics.TopicRequest;
import clone.redditcloneservice.model.topics.Topics;
import clone.redditcloneservice.model.generic_response.DataResponse;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.repository.TopicRepository;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicService {

    //todo: create business exception
    private final TopicRepository topicRepository;
    private final TopicMapper redditMapper;

    @Autowired
    TopicService(TopicRepository topicRepository, TopicMapper redditMapper) {
        this.topicRepository = topicRepository;
        this.redditMapper = redditMapper;
    }

    @Transactional
    public ResponseEntity<MessageResponse> createTopic(TopicRequest topicRequest) {
        List<Topics> topicData = topicRequest.getTopicData();
        try {
            for(Topics topics: topicData) {
                topicRepository.save(redditMapper.topicsToTopicEntity(topics));
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
        return ResponseEntity.status(HttpServletResponse.SC_CREATED).body(new MessageResponse(ValidationConstants.SUCCESS, null));
    }

    @Transactional
    public ResponseEntity<DataResponse<List<Topics>>> getAllTopics() {
        DataResponse<List<Topics>> dataResponse = new DataResponse<>();
        try {
            List<TopicEntity> allTopic = topicRepository.findAll();
            dataResponse.setData(redditMapper.topicEntityListToTopic(allTopic));

        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
        return ResponseEntity.ok(dataResponse);
    }

}
