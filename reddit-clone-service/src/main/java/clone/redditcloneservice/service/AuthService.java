package clone.redditcloneservice.service;

import clone.redditcloneservice.enums.RoleEnum;
import clone.redditcloneservice.entity.security.RoleEntity;
import clone.redditcloneservice.entity.security.UserEntity;
import clone.redditcloneservice.exception.GoogleSignInFailedException;
import clone.redditcloneservice.model.auth.GoogleSignInResponse;
import clone.redditcloneservice.model.jwt.JwtResponse;
import clone.redditcloneservice.model.auth.LoginRequest;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.model.auth.SignupRequest;
import clone.redditcloneservice.repository.RoleRepository;
import clone.redditcloneservice.repository.UserRepository;
import clone.redditcloneservice.utils.JwtUtilsService;
import clone.redditcloneservice.utils.UserDetailsImpl;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.*;

import static clone.redditcloneservice.constant.ValidationConstants.GOOGLE_SIGN_IN_FAILED;

@Slf4j
@Service
public class AuthService {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private JwtUtilsService jwtUtilsService;

    @Value("${google.client.id}")
    private String GOOGLE_CLIENT_ID;

    public ResponseEntity<?> signUpUser(SignupRequest request) {
        // check if exist user by username or email
        boolean existsByUserNameOrEmail = this.userRepository.existsByUsernameOrEmail(request.getUsername(), request.getEmail());
        if(existsByUserNameOrEmail) {
            return ResponseEntity.badRequest().body(new MessageResponse("Email or username already in use", null));
        }
        // create new user
        UserEntity userEntity = UserEntity.builder()
                .username(request.getUsername())
                .email(request.getEmail())
                .password(encoder.encode(request.getPassword()))
                .build();
        // assign roles to user by finding if it exists in our system
        Set<RoleEntity> roles = new HashSet<>();
        request.getRoles().forEach(roleName -> {
            RoleEntity roleEntity = roleRepository.findByRoleName(roleName).orElse(null);
            if(Objects.nonNull(roleEntity)) roles.add(roleEntity);
        });
        userEntity.setRoles(roles);
        UserEntity entity = userRepository.save(userEntity);
        return ResponseEntity.ok(new MessageResponse("User registered successfully", entity.getId()));
    }

    public ResponseEntity<?> saveRolesToSystem() {
        HashSet<RoleEntity> roleEntities = new HashSet<>(Arrays.asList(
                new RoleEntity(null, RoleEnum.USER),
                new RoleEntity(null, RoleEnum.ADMIN),
                new RoleEntity(null, RoleEnum.MODERATOR)
        ));
        roleRepository.saveAllAndFlush(roleEntities);
        return ResponseEntity.ok(new MessageResponse("All roles registered successfully", null));
    }

    public ResponseEntity<?> authenticate(LoginRequest request) {
        try {
            if(Objects.nonNull(request.getGoogleSignInResponse())) {
                // initiate google auth if user is already registered than returns other-wise creates a new user
                this.initiateGoogleAuth(request);
            }
            Authentication authenticationRequest =
                    UsernamePasswordAuthenticationToken.unauthenticated(request.getUsername(), request.getPassword());
            // doing authentication with username and password
            Authentication authentication =
                    this.authenticationManager.authenticate(authenticationRequest);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
            // generate token
            String jwtToken = jwtUtilsService.generateToken(userPrincipal);
            List<String> roles = userPrincipal.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();
            return ResponseEntity.ok(
                    JwtResponse.builder()
                            .id(userPrincipal.getId())
                            .token(jwtToken)
                            .email(userPrincipal.getEmail())
                            .roles(roles)
                            .build()
            );
        } catch (BadCredentialsException e) {
            return ResponseEntity.status(HttpServletResponse.SC_UNAUTHORIZED).body(new MessageResponse("Wrong username or password.", null));
        } catch (GoogleSignInFailedException e) {
            return ResponseEntity.status(HttpServletResponse.SC_UNAUTHORIZED).body(new MessageResponse(e.getErrorMessage(), null));

        }
    }

    private void initiateGoogleAuth(LoginRequest tokenRequest) {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), GsonFactory.getDefaultInstance())
                .setAudience(Collections.singletonList(GOOGLE_CLIENT_ID))
                .build();

        try {
            GoogleIdToken idToken = verifier.verify(tokenRequest.getGoogleSignInResponse().getIdToken());
            if(Objects.nonNull(idToken)) {
                // sign up user to our system
                GoogleSignInResponse googleResponse = tokenRequest.getGoogleSignInResponse();
                SignupRequest signupRequest = new SignupRequest();
                signupRequest.setUsername(googleResponse.getName());
                signupRequest.setRoles(Collections.singleton(RoleEnum.USER));
                signupRequest.setEmail(googleResponse.getEmail());
                String deterministicPassword = DigestUtils.md5DigestAsHex((googleResponse.getId() + GOOGLE_CLIENT_ID).getBytes());
                signupRequest.setPassword(deterministicPassword);

                // modify login request
                tokenRequest.setUsername(signupRequest.getUsername());
                tokenRequest.setPassword(signupRequest.getPassword());
                // Pre-check if user exists
                boolean existingUser = this.userRepository.existsByUsernameOrEmail(signupRequest.getUsername(), signupRequest.getEmail());
                if (!existingUser) {
                    // sign up method called
                    ResponseEntity<?> responseEntity = this.signUpUser(signupRequest);
                    if (responseEntity.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
                        throw new GoogleSignInFailedException(GOOGLE_SIGN_IN_FAILED);
                    }
                }
            } else {
                throw new GoogleSignInFailedException(GOOGLE_SIGN_IN_FAILED);
            }
        } catch (GeneralSecurityException | IOException e) {
            throw new GoogleSignInFailedException(GOOGLE_SIGN_IN_FAILED);
        }
    }
}
