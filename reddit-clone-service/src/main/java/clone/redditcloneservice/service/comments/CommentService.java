package clone.redditcloneservice.service.comments;

import clone.redditcloneservice.constant.ValidationConstants;
import clone.redditcloneservice.entity.comments.CommentsEntity;
import clone.redditcloneservice.entity.comments.engagement.CommentUpvotedByUser;
import clone.redditcloneservice.entity.comments.engagement.CommentsDownVotedByUser;
import clone.redditcloneservice.entity.post.PostEntity;
import clone.redditcloneservice.entity.security.UserEntity;
import clone.redditcloneservice.enums.CommentsEnum;
import clone.redditcloneservice.exception.CustomException;
import clone.redditcloneservice.mapper.UserMapper;
import clone.redditcloneservice.model.comment.CommentRequest;
import clone.redditcloneservice.model.comment.CommentResponse;
import clone.redditcloneservice.model.generic_response.DataResponse;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.model.page.PageableRequest;
import clone.redditcloneservice.repository.comment.CommentRepository;
import clone.redditcloneservice.service.PostService;
import clone.redditcloneservice.service.UserService;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CommentService {
    private final UserService userService;
    private final PostService postService;
    private final CommentRepository commentRepository;

    @Autowired
    public CommentService(UserService userService, PostService postService, CommentRepository commentRepository) {
        this.userService = userService;
        this.postService = postService;
        this.commentRepository = commentRepository;
    }

    @Transactional
    public DataResponse<MessageResponse> addComment(CommentRequest commentRequest) {
        // find user and post
        PostEntity postEntity = postService.findByPostId(commentRequest.getPostId());
        UserEntity userEntity = userService.fetchUser(commentRequest.getUserId());
        // enrich user and post
        CommentsEntity commentsEntity = new CommentsEntity();
        commentsEntity.setUser(userEntity);
        commentsEntity.setPost(postEntity);
        // if parent comment is null it means this is a first comment save it
        if(Objects.isNull(commentRequest.getParentCommentId())) {
            CommentsEntity entity = mapCommentRequestToEntity(commentsEntity, commentRequest, null);
            commentRepository.save(entity);
            return new DataResponse<>(new MessageResponse(ValidationConstants.SUCCESS,entity.getId()));
        } else {
            // if parent is not null then it means it is a reply
            // 1. find parent id set it to reply
            Optional<CommentsEntity> parentComment = commentRepository.findById(commentRequest.getParentCommentId());
            if(parentComment.isEmpty()) {
                throw new CustomException(ValidationConstants.PARENT_COMMENT_NOT_FOUND);
            } else {
                CommentsEntity entity = mapCommentRequestToEntity(commentsEntity ,commentRequest, parentComment.get());
                commentRepository.save(entity);
                return new DataResponse<>(new MessageResponse(ValidationConstants.SUCCESS,entity.getId()));
            }
        }
    }

    @Transactional
    public Page<CommentResponse> getCommentsByPostId(Long postId, PageableRequest pageableRequest) {
        // find all comments on post where parent is null
        Page<CommentsEntity> commentsEntities = commentRepository.findByPostIdAndParentIsNull(postId, pageableRequest.toPageable());
        List<CommentResponse> commentResponses = this.mapCommentEntityToResponse(commentsEntities.getContent());
        return new PageImpl<>(commentResponses, pageableRequest.toPageable(), commentsEntities.getTotalElements());
    }

    private List<CommentResponse> mapCommentEntityToResponse(List<CommentsEntity> commentsEntities) {
        List<CommentResponse> commentResponses = new ArrayList<>();
        if(commentsEntities.isEmpty()) return commentResponses;

        commentsEntities.forEach(commentsEntity ->  {
            commentResponses.add(
                    CommentResponse.builder()
                            .id(commentsEntity.getId())
                            .createdBy(commentsEntity.getCreatedBy())
                            .updatedAt(commentsEntity.getUpdatedAt())
                            .parentCommentId(Objects.nonNull(commentsEntity.getParent()) ? commentsEntity.getParent().getId():null)
                            .userResponse(UserMapper.MAPPER.mapUserEntityToUserResponse(commentsEntity.getUser()))
                            .postResponseId(commentsEntity.getPost().getId())
                            .totalUpvoteCount((long) commentsEntity.getUpVotedByUsers().size())
                            .totalDownVoteCount((long) commentsEntity.getDownVotedByUsers().size())
                            .upvotedByCurrentUser(checkEngagementForCurrentUser(commentsEntity, CommentsEnum.UP_VOTE))
                            .downVotedByCurrentUser(checkEngagementForCurrentUser(commentsEntity, CommentsEnum.DOWN_VOTE))
                            .content(commentsEntity.getContent())
                            .replies(mapCommentEntityToResponse(commentsEntity.getReplies()))
                            .build()
            );
        });
        return commentResponses;
    }


    private CommentsEntity mapCommentRequestToEntity(CommentsEntity entity, CommentRequest commentRequest, CommentsEntity parentComment) {
        entity.setParent(parentComment);
        entity.setContent(commentRequest.getContent());
        return entity;
    }

    /**
     *
     * @param commentsEntity entity from DB
     * @param type upvote or down vote
     * @return boolean if user has upvoted or down voted
     */
    private Boolean checkEngagementForCurrentUser(CommentsEntity commentsEntity, CommentsEnum type) {
        Long userId = this.userService.getCurrentLoggedInUser().getId();
        if(Objects.equals(type, CommentsEnum.UP_VOTE)) {
            return commentsEntity.getUpVotedByUsers().stream().map(CommentUpvotedByUser::getUserId).anyMatch(id -> id.equals(userId));
        } else {
            return commentsEntity.getDownVotedByUsers().stream().map(CommentsDownVotedByUser::getUserId).anyMatch(id -> id.equals(userId));
        }
    }
}
