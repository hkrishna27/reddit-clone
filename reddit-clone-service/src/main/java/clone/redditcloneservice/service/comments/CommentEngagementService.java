package clone.redditcloneservice.service.comments;

import clone.redditcloneservice.constant.ValidationConstants;
import clone.redditcloneservice.entity.comments.CommentsEntity;
import clone.redditcloneservice.entity.comments.engagement.CommentUpvotedByUser;
import clone.redditcloneservice.entity.comments.engagement.CommentsDownVotedByUser;
import clone.redditcloneservice.exception.CustomException;
import clone.redditcloneservice.model.comment.engagement.CommentEngagement;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.repository.comment.CommentRepository;
import clone.redditcloneservice.service.UserService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class CommentEngagementService {
    private final CommentRepository commentRepository;
    private final UserService userService;

    @Autowired
    public CommentEngagementService(CommentRepository commentRepository, UserService userService) {
        this.commentRepository = commentRepository;
        this.userService = userService;
    }

    @Transactional
    public ResponseEntity<MessageResponse> performEngagementOpsOnComments(CommentEngagement commentEngagement) {
        // find the comment and update upvote or down-vote
        CommentsEntity commentsEntity = commentRepository.findById((commentEngagement.getCommentId())).orElse(null);
        if(Objects.nonNull(commentsEntity)) {
            Long userId = userService.getCurrentLoggedInUser().getId();
            if(commentEngagement.isAddNewEngagement()) {
                if(commentEngagement.isUpVoted()) {
                    commentsEntity.getUpVotedByUsers().add(new CommentUpvotedByUser(userId,commentsEntity));
                } else if (commentEngagement.isDownVoted()){
                    commentsEntity.getDownVotedByUsers().add(new CommentsDownVotedByUser(userId,commentsEntity));
                }
            }
            else if(commentEngagement.isSwitchExistingEngagement()) {
                switchEngagementOfCurrentUserFromComment(commentEngagement,commentsEntity,userId);
            } else if (commentEngagement.isRemoveExistingEngagement()) {
                removeEngagementOfCurrentUserFromComment(commentEngagement,commentsEntity,userId);
            }
            commentRepository.save(commentsEntity);
            return ResponseEntity.ok(new MessageResponse(ValidationConstants.SUCCESS, commentsEntity.getId()));
        }
        throw new CustomException(ValidationConstants.COMMENT_NOT_FOUND);
    }

    private void removeEngagementOfCurrentUserFromComment(CommentEngagement commentEngagement, CommentsEntity commentsEntity, Long userId) {
        if (commentEngagement.isUpVoted()) {
            commentsEntity.getUpVotedByUsers().removeIf(e -> e.getUserId().equals(userId));
        } else if (commentEngagement.isDownVoted()) {
            commentsEntity.getDownVotedByUsers().removeIf(e -> e.getUserId().equals(userId));
        }
    }

    private void switchEngagementOfCurrentUserFromComment(CommentEngagement commentEngagement, CommentsEntity commentsEntity, Long userId) {
        // if existing engagement on comment is there then remove it
        if (commentEngagement.isUpVoted()) {
            // remove down vote
            commentsEntity.getDownVotedByUsers().removeIf(e -> e.getUserId().equals(userId));
            // add new upvote
            commentsEntity.getUpVotedByUsers().add(new CommentUpvotedByUser(userId,commentsEntity));
        } else if (commentEngagement.isDownVoted()) {
            // remove upvote
            commentsEntity.getUpVotedByUsers().removeIf(e -> e.getUserId().equals(userId));
            // add down vote
            commentsEntity.getDownVotedByUsers().add(new CommentsDownVotedByUser(userId,commentsEntity));
        }
    }
}
