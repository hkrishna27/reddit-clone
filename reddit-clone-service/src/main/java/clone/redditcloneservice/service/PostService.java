package clone.redditcloneservice.service;

import clone.redditcloneservice.constant.PostConstants;
import clone.redditcloneservice.constant.ValidationConstants;
import clone.redditcloneservice.entity.post.PostEntity;
import clone.redditcloneservice.enums.PostCatagoryEnum;
import clone.redditcloneservice.enums.PostTypeEnum;
import clone.redditcloneservice.exception.CustomException;
import clone.redditcloneservice.mapper.PostMapper;
import clone.redditcloneservice.model.generic_response.DataResponse;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.model.page.PageableRequest;
import clone.redditcloneservice.model.post.PostRequest;
import clone.redditcloneservice.model.post.PostResponse;
import clone.redditcloneservice.model.post.engagement.PostEngagement;
import clone.redditcloneservice.repository.CommunityRepository;
import clone.redditcloneservice.repository.PostRepository;
import clone.redditcloneservice.repository.comment.CommentRepository;
import clone.redditcloneservice.utils.UserDetailsImpl;
import jakarta.persistence.Tuple;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostService {
    private final PostRepository postRepository;
    private final PostMapper postsMapper;
    private final CommunityRepository communityRepository;
    private final UserService userService;
    private final PostEngagementService postEngagementService;
    private final CommentRepository commentRepository;

    @Autowired
    public PostService(PostMapper postsMapper, PostRepository postRepository, CommunityRepository communityRepository, UserService service, UserService userService, PostEngagementService postEngagementService, CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.postsMapper = postsMapper;
        this.communityRepository = communityRepository;
        this.userService = userService;
        this.postEngagementService = postEngagementService;
        this.commentRepository = commentRepository;
    }

    public ResponseEntity<DataResponse<MessageResponse>> createRedditPost(PostRequest postRequest) {
        if(!communityRepository.existsById(postRequest.getCommunityId())) {
            throw new CustomException("Provided community does not exist");
        }
        PostTypeEnum postType = postRequest.getPostType();
        Long postId;
        switch (postType) {
            case TEXT -> postId = createTextPost(postRequest);
            case IMAGE_AND_VIDEO -> postId = createMultimediaPost(postRequest);
            case LINK -> postId = createExternalLinkPost(postRequest);
            default -> throw new CustomException("No post type mentioned in request");
        }
        return ResponseEntity.ok(new DataResponse<>(new MessageResponse(ValidationConstants.SUCCESS, postId)));
    }

    @Transactional
    protected Long createExternalLinkPost(PostRequest postRequest) {
        if (!isExternalLinkAvailable(postRequest)) {
            throw new CustomException("No external link provided");
        } else if(isMultimediaAvailable(postRequest) || isTextBodyAvailable(postRequest)) {
            throw new CustomException("Request has other parameters other than title and external link");
        } else {
            PostEntity postEntity = this.postsMapper.postRequestToPostRequestEntity(postRequest, new PostEntity());
            postEntity.setUserEntity(userService.fetchUser(postRequest.getUserId()));
            return postRepository.save(postEntity).getId();
        }
    }

    @Transactional
    protected Long createMultimediaPost(PostRequest postRequest) {
        if (!isMultimediaAvailable(postRequest)) {
            throw new CustomException("No multimedia provided");
        } else if(isTextBodyAvailable(postRequest) || isExternalLinkAvailable(postRequest)) {
            throw new CustomException("Request has other parameters other than title and multimedia");
        } else {
            PostEntity postRequestEntity = this.postsMapper.postRequestToPostRequestEntity(postRequest, new PostEntity());
            postRequestEntity.setUserEntity(userService.fetchUser(postRequest.getUserId()));
            // map child to parent
            if(Objects.nonNull(postRequestEntity.getVideoUrls())) {
                postRequestEntity.setVideoUrls(postRequestEntity.getVideoUrls().stream().peek(video-> video.setPostRequestEntity(postRequestEntity)).toList());
            }
            // map child to parent
            if(Objects.nonNull(postRequestEntity.getImageUrls())) {
                postRequestEntity.setImageUrls(postRequestEntity.getImageUrls().stream().peek(image-> image.setPostRequestEntity(postRequestEntity)).toList());
            }
            return postRepository.save(postRequestEntity).getId();
        }
    }

    @Transactional
    protected Long createTextPost(PostRequest postRequest) {
        if (!isTextBodyAvailable(postRequest)) {
            throw new CustomException("No text body provided");
        } else if(isMultimediaAvailable(postRequest) || isExternalLinkAvailable(postRequest)) {
            throw new CustomException("Request has other parameters other than title and text body");
        } else {
            PostEntity postEntity = this.postsMapper.postRequestToPostRequestEntity(postRequest, new PostEntity());
            postEntity.setUserEntity(userService.fetchUser(postRequest.getUserId()));
            return postRepository.save(postEntity).getId();
        }
    }

    @Transactional
    public ResponseEntity<Page<PostResponse>> getPostsByCommunityId(Long communityId, PageableRequest pageableRequest) {
        // fetch all post
        Page<PostEntity> postRequestEntity = this.postRepository.findAllByCommunityId(communityId , pageableRequest.toPageable());
        List<PostEntity> postEntities = postRequestEntity.getContent();
        // perform enriching
        List<PostResponse> mappedFinalResponse = this.enrichData(postEntities, communityId);
        return ResponseEntity.ok(new PageImpl<>(mappedFinalResponse, pageableRequest.toPageable(), postRequestEntity.getTotalElements()));
    }

    public List<PostResponse> enrichData(List<PostEntity> postEntities, Long communityId) {
        // find all post ids
        List<Long> postIds = postEntities.stream().map(PostEntity::getId).toList();
        Map<Long,Map<String,Long>> votesCount =  this.createHashmapForUpvoteAndDownVoteCount(this.postEngagementService.getTotalUpVotesAndDownVotes(postIds, communityId));
        // find if current user has up-voted or down-voted
        UserDetailsImpl currentUserPrincipal = this.userService.getCurrentLoggedInUser();
        Map<Long, PostEngagement> engagementCurrentUser;
        if(Objects.nonNull(currentUserPrincipal)) {
            engagementCurrentUser = fetchEngagementDataForPostCurrentUser(postIds,communityId,currentUserPrincipal.getId());
        } else {
            engagementCurrentUser = new HashMap<>();
        }
        List<PostResponse> postResponse = this.postsMapper.postEntityListToPostResponseList(postEntities);
        // map upvote count to response
        return postResponse.stream()
                .peek(post -> {
                    // for each post fetch comments count
                    post.setCommentsCount(this.commentRepository.countByPost_Id(post.getId()));
                    if (Objects.nonNull(engagementCurrentUser.get(post.getId()))) {
                        post.setCurrentUserPostEngagementId(engagementCurrentUser.get(post.getId()).getId());
                        post.setCurrentUserUpVoted(engagementCurrentUser.get(post.getId()).isUpVoted());
                        post.setCurrentUserDownVoted(engagementCurrentUser.get(post.getId()).isDownVoted());
                    }
                    if(Objects.nonNull(votesCount.get(post.getId()))) {
                        post.setUpVotesCount(votesCount.get(post.getId()).get(PostConstants.UP_VOTES_COUNT));
                        post.setDownVotesCount(votesCount.get(post.getId()).get(PostConstants.DOWN_VOTES_COUNT));
                    }

                })
                .collect(Collectors.toList());
    }

    private Map<Long, PostEngagement> fetchEngagementDataForPostCurrentUser(List<Long> postIds, Long communityId, Long id) {
        List<PostEngagement> listResponseEntity = postEngagementService.fetchEngagementDataForPostWithUser(postIds, communityId, id);
        return listResponseEntity.stream().collect(Collectors.toUnmodifiableMap(
                PostEngagement::getPostId,
                postEngagement -> postEngagement
        ));
    }

    private Map<Long, Map<String,Long>> createHashmapForUpvoteAndDownVoteCount(List<Tuple> totalVotesTuple) {
        return totalVotesTuple.stream().collect(Collectors.toUnmodifiableMap(
                postId -> postId.get(0,Long.class),
                count -> new HashMap<>()
                        {{
                            put(PostConstants.UP_VOTES_COUNT,count.get(1, Long.class));
                            put(PostConstants.DOWN_VOTES_COUNT,count.get(2, Long.class));
                        }}
        ));
    }

    @Transactional
    protected boolean isMultimediaAvailable(PostRequest postRequest) {
        return Objects.nonNull(postRequest.getImageUrls()) && !postRequest.getImageUrls().isEmpty()
                || (Objects.nonNull(postRequest.getVideoUrls()) && !postRequest.getVideoUrls().isEmpty());
    }

    @Transactional
    protected boolean isExternalLinkAvailable(PostRequest postRequest) {
        return Objects.nonNull(postRequest.getLink()) && (!postRequest.getLink().isEmpty());
    }

    @Transactional
    protected boolean isTextBodyAvailable(PostRequest postRequest) {
        return Objects.nonNull(postRequest.getTextBody()) && (!postRequest.getTextBody().isEmpty());
    }

    public boolean existsByPostId(Long postId) {
        return postRepository.existsById(postId);
    }

    public PostEntity findByPostId(Long postId) {
        PostEntity postEntity = postRepository.findById(postId).orElse(null);
        if(Objects.isNull(postEntity)) {
            throw new CustomException(ValidationConstants.NO_SUCH_POST);
        } else {
            return postEntity;
        }
    }

    public PostResponse findSinglePostData(Long postId, Long communityId) {
        PostEntity postEntity = this.findByPostId(postId);
        List<PostResponse> postResponses = this.enrichData(List.of(postEntity), communityId);
        return postResponses.stream().findFirst().orElse(null);
    }

    public ResponseEntity<Page<PostResponse>> getPostsByCategory(PostCatagoryEnum postCategory, PageableRequest pageableRequest) {
        // fetch all post on basis of category
        Page<PostEntity> postRequestEntity = null;
        if(postCategory.equals(PostCatagoryEnum.HOME)) {
           postRequestEntity = this.postRepository.findLatestPosts(pageableRequest.toPageable());
        } else if (postCategory.equals(PostCatagoryEnum.TRENDING)) {
            postRequestEntity = this.postRepository.findTrendingPosts(pageableRequest.toPageable());
        }

        if(Objects.nonNull(postRequestEntity)) {
            List<PostEntity> postEntities = postRequestEntity.getContent();
            // perform enriching
            List<PostResponse> mappedFinalResponse = this.enrichData(postEntities, null);
            return ResponseEntity.ok(new PageImpl<>(mappedFinalResponse, pageableRequest.toPageable(), postRequestEntity.getTotalElements()));
        }
        return ResponseEntity.ok(new PageImpl<>(new ArrayList<>(), pageableRequest.toPageable(), BigInteger.ZERO.longValue()));
    }

    public ResponseEntity<Page<PostResponse>> getPostsByCategoryType(String categoryType, PageableRequest pageableRequest) {
        Page<PostEntity> postRequestEntity = this.postRepository.findPostsByCategoryType(categoryType,pageableRequest.toPageable());
        List<PostEntity> postEntities = postRequestEntity.getContent();
        // perform enriching
        List<PostResponse> mappedFinalResponse = this.enrichData(postEntities, null);
        return ResponseEntity.ok(new PageImpl<>(mappedFinalResponse, pageableRequest.toPageable(), postRequestEntity.getTotalElements()));
    }
}
