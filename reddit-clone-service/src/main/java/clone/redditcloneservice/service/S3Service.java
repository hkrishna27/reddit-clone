package clone.redditcloneservice.service;

import clone.redditcloneservice.model.s3.SignedUrlRequestS3;
import clone.redditcloneservice.model.s3.SignedUrlResponseS3;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.S3Object;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Log4j2
public class S3Service {

    private final AmazonS3 s3client;

    @Value("${aws.s3.bucket}")
    private String bucketName;

    public S3Service(AmazonS3 s3client) {
        this.s3client = s3client;
    }

    public void uploadFile(String keyName, MultipartFile file) throws IOException {
        var putObjectResult = s3client.putObject(bucketName, keyName, file.getInputStream(), null);
        log.info(putObjectResult.getMetadata());
    }

    public S3Object getFile(String keyName) {
        return s3client.getObject(bucketName, keyName);
    }

    public SignedUrlResponseS3 generateSignedUrlS3(SignedUrlRequestS3 request) {
        // Combine folder and file name to form the object key
        String objectKey = request.getS3BucketFolder() + "/" + request.getFileName();
        GeneratePresignedUrlRequest signedUrlRequest = new GeneratePresignedUrlRequest(bucketName,objectKey,request.getHttpMethod());
        return SignedUrlResponseS3.builder().signedUrl(s3client.generatePresignedUrl(signedUrlRequest).toString()).build();
    }

    public List<SignedUrlResponseS3> generateSignedUrlsS3(List<SignedUrlRequestS3> requests) {
        // Combine folder and file name to form the object key
        List<SignedUrlResponseS3> res = new ArrayList<>();
        requests.forEach(request-> {
            String objectKey = request.getS3BucketFolder() + "/" + request.getFileName();
            GeneratePresignedUrlRequest signedUrlRequest = new GeneratePresignedUrlRequest(bucketName,objectKey,request.getHttpMethod());
            res.add(SignedUrlResponseS3.builder().fileName(request.getFileName()).signedUrl(s3client.generatePresignedUrl(signedUrlRequest).toString()).build());
        });
        return res;
    }
}