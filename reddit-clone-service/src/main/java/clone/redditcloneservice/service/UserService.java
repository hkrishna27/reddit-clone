package clone.redditcloneservice.service;

import clone.redditcloneservice.constant.ValidationConstants;
import clone.redditcloneservice.entity.security.RoleEntity;
import clone.redditcloneservice.entity.security.UserEntity;
import clone.redditcloneservice.enums.RoleEnum;
import clone.redditcloneservice.exception.CustomException;
import clone.redditcloneservice.model.auth.UserResponse;
import clone.redditcloneservice.repository.UserRepository;
import clone.redditcloneservice.utils.UserDetailsImpl;
import clone.redditcloneservice.utils.UserServiceImpl;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public UserDetailsImpl getCurrentLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(Objects.nonNull(authentication)) {
            return (UserDetailsImpl) authentication.getPrincipal();
        }
        return null;
    }

    @Transactional
    public UserEntity fetchUser(Long userId) {
        UserEntity user = userRepository.findById(userId).orElse(null);
        if(Objects.nonNull(user)) {
            return user;
        } else {
            throw new CustomException(ValidationConstants.NO_SUCH_USER);
        }
    }

    @Transactional
    public boolean existsByUserId(Long userId) {
        return userRepository.existsById(userId);
    }

    @Transactional
    public UserEntity saveOrUpdateUser(UserEntity user) {
        return userRepository.save(user);
    }

    public UserResponse getCurrentLoggedUserResponseModel() {
        UserDetailsImpl currentLoggedInUser = getCurrentLoggedInUser();
        return UserResponse.builder()
                .userId(currentLoggedInUser.getId())
                .userName(currentLoggedInUser.getUsername())
                .userEmail(currentLoggedInUser.getEmail())
                .userRoles(currentLoggedInUser.getAuthorities().stream().map(authority-> RoleEnum.valueOf(authority.getAuthority())).toList())
                .build();
    }
}
