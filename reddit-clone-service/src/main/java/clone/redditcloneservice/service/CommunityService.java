package clone.redditcloneservice.service;

import clone.redditcloneservice.constant.ValidationConstants;
import clone.redditcloneservice.entity.community.CommunityEntity;
import clone.redditcloneservice.entity.community.CommunityTopicEntity;
import clone.redditcloneservice.entity.security.UserEntity;
import clone.redditcloneservice.enums.CommunityEnum;
import clone.redditcloneservice.exception.CustomException;
import clone.redditcloneservice.mapper.CommunityMapper;
import clone.redditcloneservice.model.community.CommunityRequest;
import clone.redditcloneservice.model.community.CommunityResponse;
import clone.redditcloneservice.model.generic_response.MessageResponse;
import clone.redditcloneservice.repository.CommunityRepository;
import clone.redditcloneservice.repository.UserRepository;
import clone.redditcloneservice.utils.UserDetailsImpl;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CommunityService {
    private final CommunityRepository communityRepository;
    private final UserService userService;
    private final EntityManager entityManager;

    @Autowired
    public CommunityService(CommunityMapper communityMapper, CommunityRepository communityRepository, UserService userService, EntityManager entityManager) {
        this.communityRepository = communityRepository;
        this.userService = userService;
        this.entityManager = entityManager;
    }

    @Transactional
    public ResponseEntity<MessageResponse> createCommunity(CommunityRequest communityRequest) {
        CommunityEntity communityEntity = CommunityMapper.MAPPER.mapCommunityRequestToEntity(communityRequest);
        // map child to parent
        communityEntity.setTopicEntities(this.mapChildToParent(communityEntity));
        try {
            CommunityEntity entity = communityRepository.save(communityEntity);
            return ResponseEntity.ok(new MessageResponse(ValidationConstants.SUCCESS, entity.getId()));
        } catch (Exception exception) {
            return ResponseEntity
                    .status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
                    .body(new MessageResponse(ValidationConstants.SOMETHING_WENT_WRONG, null));

        }
    }

    private List<CommunityTopicEntity> mapChildToParent(CommunityEntity communityEntity) {
        return communityEntity.getTopicEntities().stream().peek(communityTopicEntity -> communityTopicEntity.setCommunityEntity(communityEntity)).toList();
    }

    @Transactional
    public ResponseEntity<List<CommunityResponse>> fetchAllCommunities() {
        List<CommunityEntity> communityEntities = communityRepository.findAll();
        List<CommunityResponse>communityResponseList = CommunityMapper.MAPPER.mapCommunityEntityToCommunityResponseModel(communityEntities);
        return ResponseEntity.ok(communityResponseList);
    }

    @Transactional
    public ResponseEntity<Boolean> existsByCommunityName(String communityName) {
        return ResponseEntity.ok(
                this.communityRepository.existsByCommunityName(communityName)
        );
    }

    @Transactional
    public ResponseEntity<CommunityResponse> fetchCommunityById(Long communityId) {
        CommunityEntity communityEntity = communityRepository.findById(communityId).orElse(null);
        if(Objects.nonNull(communityEntity)) {
            return ResponseEntity.ok(this.mapCommunityEntityToResponse(communityEntity));
        }
        throw new CustomException(ValidationConstants.NO_SUCH_COMMUNITY);
    }

    private boolean checkIfCurrentUserJoinedCommunity(CommunityEntity communityEntity, UserDetailsImpl currentLoggedInUser) {
        return communityEntity.getCommunityMembers().stream().anyMatch(user -> user.getId().equals(currentLoggedInUser.getId()));
    }

    @Transactional
    public ResponseEntity<MessageResponse> joinCommunity(Long userId, Long communityId, CommunityEnum operation) {
        CommunityEntity community = communityRepository.findById(communityId).orElse(null);
        UserEntity user = userService.fetchUser(userId);
        if (Objects.nonNull(community) && Objects.nonNull(user)) {
            if(operation.equals(CommunityEnum.JOIN)) {
                user.getCommunities().add(community);
                community.getCommunityMembers().add(user);
            } else if (operation.equals(CommunityEnum.UN_JOIN)) {
                user.getCommunities().remove(community);
                community.getCommunityMembers().remove(user);
            } else {
                throw new CustomException(ValidationConstants.OPERATION_ERROR);
            }
            userService.saveOrUpdateUser(user);
            communityRepository.save(community);
            return ResponseEntity.ok(new MessageResponse(ValidationConstants.SUCCESS,null));
        }
        throw new CustomException(ValidationConstants.NO_SUCH_COMMUNITY_OR_USER);
    }

    private CommunityResponse mapCommunityEntityToResponse(CommunityEntity communityEntity) {
        CommunityResponse communityResponse = CommunityMapper.MAPPER.mapCommunityEntityToCommunityResponseModel(communityEntity);
        // check if current user joined this community or not
        if(checkIfCurrentUserJoinedCommunity(communityEntity, userService.getCurrentLoggedInUser())) {
            communityResponse.setLoggedUserMember(userService.getCurrentLoggedUserResponseModel());
        }
        communityResponse.setTotalCommunityMembers(communityEntity.getCommunityMembers().size());
        return communityResponse;
    }

    public List<CommunityResponse> findCommunitiesMatchesLike(String searchText, Integer limit) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<CommunityEntity> criteriaQuery = cb.createQuery(CommunityEntity.class);
        Root<CommunityEntity> root = criteriaQuery.from(CommunityEntity.class);
        Predicate communityPredicate = cb.like(root.get("communityName"), "%" + searchText + "%");
        criteriaQuery.where(communityPredicate);
        TypedQuery<CommunityEntity> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setMaxResults(limit);
        return typedQuery.getResultList().stream().map(this::mapCommunityEntityToResponse).toList();
    }
}
