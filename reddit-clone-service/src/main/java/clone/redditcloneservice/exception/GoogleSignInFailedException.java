package clone.redditcloneservice.exception;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoogleSignInFailedException extends  RuntimeException {
    private String errorMessage;

    GoogleSignInFailedException() {
        super();
    }

    public GoogleSignInFailedException(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
