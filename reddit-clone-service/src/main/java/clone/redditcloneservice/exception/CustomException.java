package clone.redditcloneservice.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomException extends  RuntimeException {
    private String errorMessage;

    CustomException() {
        super();
    }

    public CustomException(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
