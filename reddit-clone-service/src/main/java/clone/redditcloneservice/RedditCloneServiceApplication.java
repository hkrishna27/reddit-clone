package clone.redditcloneservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedditCloneServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedditCloneServiceApplication.class, args);
	}

}
