package clone.redditcloneservice.repository.comment;

import clone.redditcloneservice.entity.comments.CommentsEntity;
import clone.redditcloneservice.entity.post.PostEntity;
import clone.redditcloneservice.model.post.PostResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<CommentsEntity,Long> {
    Optional<CommentsEntity> findByParent(CommentsEntity commentsEntity);

    Page<CommentsEntity> findByPostIdAndParentIsNull(Long postId, Pageable pageable);

    Long countByPost_Id(Long id);
}
