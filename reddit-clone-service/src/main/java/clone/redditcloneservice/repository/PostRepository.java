package clone.redditcloneservice.repository;

import clone.redditcloneservice.entity.post.PostEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface PostRepository extends JpaRepository<PostEntity,Long> {
    Page<PostEntity> findAllByCommunityId(Long communityId, Pageable pageable);

    @Query("SELECT p FROM PostEntity p ORDER BY p.createdAt DESC")
    Page<PostEntity> findLatestPosts(Pageable pageable);

    @Query("SELECT p " +
            "FROM PostEntity p " +
            "INNER JOIN PostEngagementEntity pe ON p.id = pe.postId " +
            "GROUP BY p.id " +
            "ORDER BY COUNT(CASE WHEN pe.upVoted = TRUE THEN 1 END) DESC")
    Page<PostEntity> findTrendingPosts(Pageable pageable);

    @Query("SELECT p " +
            "FROM PostEntity p " +
            "LEFT JOIN PostEngagementEntity pe ON p.id = pe.postId " +
            "WHERE LOWER(p.textTitle) LIKE LOWER(CONCAT('%', :categoryType, '%')) " +
            "   OR LOWER(p.textBody) LIKE LOWER(CONCAT('%', :categoryType, '%')) " +
            "GROUP BY p.id " +
            "ORDER BY COUNT(CASE WHEN pe.upVoted = TRUE THEN 1 END) DESC")
    Page<PostEntity> findPostsByCategoryType(@Param("categoryType") String categoryType, Pageable pageable);
}
