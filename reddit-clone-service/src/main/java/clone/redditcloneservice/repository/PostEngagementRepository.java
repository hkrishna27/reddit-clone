package clone.redditcloneservice.repository;

import clone.redditcloneservice.entity.post.engagement.PostEngagementEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostEngagementRepository extends JpaRepository<PostEngagementEntity,Long> {
}
