package clone.redditcloneservice.repository;

import clone.redditcloneservice.enums.RoleEnum;
import clone.redditcloneservice.entity.security.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity,Long> {
    Optional<RoleEntity> findByRoleName(RoleEnum name);
}
