package clone.redditcloneservice.repository;

import clone.redditcloneservice.entity.community.CommunityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommunityRepository extends JpaRepository<CommunityEntity,Long> {
    boolean existsByCommunityName(String communityName);
}
