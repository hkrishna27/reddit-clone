package clone.redditcloneservice;

import clone.redditcloneservice.entity.security.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestH2 extends JpaRepository<UserEntity, Long> {

}