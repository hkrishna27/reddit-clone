package clone.redditcloneservice;

import clone.redditcloneservice.entity.security.UserEntity;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

//for assigning random port while running testing server
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RedditCloneServiceApplicationTests {

	private final TestH2 testH2;

	@Autowired
	public RedditCloneServiceApplicationTests(TestH2 testH2) {
		this.testH2 = testH2;
	}

	@Test
	@Disabled
	public void testH2() {
		UserEntity userEntity = UserEntity.builder()
				.Id(1L)
				.email("test@test.com")
				.password("dummyPass")
				.username("dummyuser")
				.roles(null)
				.build();

		testH2.save(userEntity);
	}
}
