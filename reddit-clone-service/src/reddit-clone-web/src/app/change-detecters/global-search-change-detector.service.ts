import { Injectable } from '@angular/core';
import { NonEnrichedSearchResponse } from 'generated/angular-client';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalSearchChangeDetectorService {
  public nonEnrichedSearchResponseSubject$ = new BehaviorSubject<NonEnrichedSearchResponse>({});
  public nonEnrichedSearchResponse$ : Observable<NonEnrichedSearchResponse> = this.nonEnrichedSearchResponseSubject$.asObservable();
  constructor() { }

  setNonEnrichedGlobalSearchData(nonEnrichedSearchResponse: NonEnrichedSearchResponse) {
    this.nonEnrichedSearchResponseSubject$.next(nonEnrichedSearchResponse);
  }
}
