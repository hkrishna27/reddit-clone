
import { Injectable } from '@angular/core';
import { CommunityResponse } from 'generated/angular-client';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommunityChangeDetecterService {
  public communityDataSubject$ = new BehaviorSubject<CommunityResponse[]>([]);
  public communityData$: Observable<CommunityResponse[]> =
    this.communityDataSubject$.asObservable();

  public singleCommunityDataSubject$ = new BehaviorSubject<CommunityResponse>(
    {}
  );
  public singleCommunityData$: Observable<CommunityResponse> =
    this.singleCommunityDataSubject$.asObservable();

  public communityIdSubject$ = new BehaviorSubject<number>(NaN);
  public communityId$: Observable<number> =
    this.communityIdSubject$.asObservable();

  constructor() { }

  public setCommunityData(communityData: CommunityResponse[]) {
    this.communityDataSubject$.next(communityData);
  }

  public setSingleCommunityData(communityData: CommunityResponse) {
    this.singleCommunityDataSubject$.next(communityData);
  }

  public setCommunityId(communityId: number) {
    this.communityIdSubject$.next(communityId);
  }
}
