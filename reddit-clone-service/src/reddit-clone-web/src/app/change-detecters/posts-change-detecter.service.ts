import { Injectable } from '@angular/core';
import { PagePostResponse } from 'generated/angular-client';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsChangeDetecterService {
  public postDataSubject$ = new BehaviorSubject<PagePostResponse>({});
  public postData$ : Observable<PagePostResponse> = this.postDataSubject$.asObservable();

  constructor() { }

  setPostData(postData: PagePostResponse) {
    this.postDataSubject$.next(postData);
  }
}
