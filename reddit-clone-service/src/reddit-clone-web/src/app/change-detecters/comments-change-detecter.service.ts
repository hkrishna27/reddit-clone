import { Injectable } from '@angular/core';
import { PageCommentResponse } from 'generated/angular-client';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentsChangeDetecterService {
  public commentsDataSubject$ = new BehaviorSubject<PageCommentResponse>({});
  public commentsData$ : Observable<PageCommentResponse> = this.commentsDataSubject$.asObservable();

  constructor() { }

  setCommentsData(comments: PageCommentResponse) {
    this.commentsDataSubject$.next(comments);
  }
}
