import { BehaviorSubject, Observable } from "rxjs";
import { PostCatagory } from "../shared/enums/catagory.enum";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class CatagoryChangeDetecterService {
  public catagoryDataSubject$ = new BehaviorSubject<PostCatagory>(undefined);
  public catagoryData$ : Observable<PostCatagory> = this.catagoryDataSubject$.asObservable();

  constructor() { }

  setCatagoryData(catagory: PostCatagory) {
    this.catagoryDataSubject$.next(catagory);
  }
}
