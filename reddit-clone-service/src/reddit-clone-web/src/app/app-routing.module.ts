import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { TrendingComponent } from './components/trending/trending.component';
import { CategoryComponent } from './components/category/category.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent },
    { path: 'trending', component: TrendingComponent },
    { path: 'catagory/:name', component: CategoryComponent },
    {
      path: 'community',
      loadChildren: () => import('./components/community/community.module').then(m => m.CommunityModule)
    },
    {
      path: 'comments',
      loadChildren: () => import('./components/comments/comments.module').then(m => m.CommentsModule)
    },
    {
      path: 'post',
      loadChildren: () => import('./components/posts/posts.module').then(m => m.PostsModule)
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
