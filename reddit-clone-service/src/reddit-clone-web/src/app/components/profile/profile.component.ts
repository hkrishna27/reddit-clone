import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';
import { OverlayService } from 'src/app/shared/services/overlay.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userName$!: Observable<any>;
  isAuthenticated$!: Observable<boolean>;

  constructor(
    private authWebService: AuthWebService,
    private overlayService: OverlayService
  ) { }

  ngOnInit(): void {
    this.isAuthenticated$ = this.authWebService.isAuthenticated$;
    this.userName$ = this.authWebService.currentUser$;
  }

  logout() {
    this.authWebService.clearSession();
    this.overlayService.closeOverlay();
  }

}
