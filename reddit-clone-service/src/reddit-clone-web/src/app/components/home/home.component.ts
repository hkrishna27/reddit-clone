
import { Component, OnInit } from '@angular/core';
import { CatagoryChangeDetecterService } from 'src/app/change-detecters/catagory-change-detector.service';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';
import { HOME_CATAGORY } from 'src/app/shared/enums/catagory.enum';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private readonly catagoryCd: CatagoryChangeDetecterService,private readonly communityCd: CommunityChangeDetecterService) {}

  ngOnInit(): void {
    this.communityCd.setCommunityId(NaN);
    this.fetchPostsForHomeCatagory()
  }

  fetchPostsForHomeCatagory() {
    this.catagoryCd.setCatagoryData(HOME_CATAGORY)
  }
 }

