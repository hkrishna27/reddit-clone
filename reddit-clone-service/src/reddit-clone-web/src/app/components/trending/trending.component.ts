import { Component, OnInit } from '@angular/core';
import { CatagoryChangeDetecterService } from 'src/app/change-detecters/catagory-change-detector.service';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';
import { TRENDING_CATAGORY } from 'src/app/shared/enums/catagory.enum';

@Component({
  selector: 'app-trending',
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.scss']
})
export class TrendingComponent implements OnInit {
  constructor(private readonly catagoryCd: CatagoryChangeDetecterService,private readonly communityCd: CommunityChangeDetecterService) {}

  ngOnInit(): void {
    this.communityCd.setCommunityId(NaN);
    this.fetchPostsForHomeCatagory()
  }

  fetchPostsForHomeCatagory() {
    this.catagoryCd.setCatagoryData(TRENDING_CATAGORY)
  }
}
