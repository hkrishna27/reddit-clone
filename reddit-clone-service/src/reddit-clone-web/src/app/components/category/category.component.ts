import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CatagoryChangeDetecterService } from 'src/app/change-detecters/catagory-change-detector.service';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  category!: string | null;

  constructor(
    private readonly route:ActivatedRoute,
    private readonly catagoryCd: CatagoryChangeDetecterService,
    private readonly communityCd: CommunityChangeDetecterService ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.category = params.get('name');
      this.communityCd.setCommunityId(NaN);
      this.catagoryCd.setCatagoryData(undefined);
    });
  }

}
