import { Component, Input, OnInit } from '@angular/core';
import { CommentControllerService, CommentRequest, CommentsEngagementControllerService } from 'generated/angular-client';
import { Subject, takeUntil } from 'rxjs';
import { Comments } from '../../posts/model/post.model';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';
import { CommentSharedService } from '../comment.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';
import { CommentEngagement } from 'generated/angular-client';


@Component({
  selector: 'app-reddit-comments',
  templateUrl: './reddit-comments.component.html',
  styleUrls: ['./reddit-comments.component.scss'],
})
export class RedditCommentsComponent implements OnInit {

  @Input() comment!: Comments;
  showReplies: boolean = true;
  showReplyEditor = false;
  replyContent = '';
  isExpanded = false;
  upward: REDDIT_CONSTANT = REDDIT_CONSTANT.UPWARD;
  downward: REDDIT_CONSTANT = REDDIT_CONSTANT.DOWNWORD;
  private unsubscribe$ = new Subject<void>();

  toggleExpand() {
    this.comment.isExpanded = !this.comment.isExpanded;
  }

  constructor(
    public sharedService: SharedService,
    private authWebService: AuthWebService,
    private commentService: CommentControllerService,
    private commentSharedService: CommentSharedService,
    private commentEngagementService: CommentsEngagementControllerService
  ) {}

  ngOnInit(): void {}

  /** emmited event from editor to show reply box or not */
  emittedShowReplyEditorEvent(showReplyEditor: boolean) {
    this.showReplyEditor = showReplyEditor;
  }

  /**
   * catches comment event
   * @param comment emmited comment from reply box
   */
  emittedComment(reply: string, comment:any) {
    console.log(reply, comment);
    const payload: CommentRequest = {
      postId: comment.postResponseId,
      parentCommentId: comment.id,
      userId: this.authWebService.getCurrentUserValue().id,
      content: reply
    }
   this.commentService.addComment(payload).pipe(
    takeUntil(this.unsubscribe$)
   ).subscribe((_success) => {
      comment.replies?.push(this.commentSharedService.createCommentResponseModel(reply,comment,_success.data?.id!));
      console.log(comment);
      this.showReplyEditor = false;
   });
  }

  upvoteOrDownComment(comment: Comments,ops: REDDIT_CONSTANT) {
    this.createEngagement(comment,ops);
  }

  createEngagement(comment: Comments, ops: REDDIT_CONSTANT) {
    const mockReq = Object.assign({}, comment);
    let payload: CommentEngagement;
    switch (ops) {
      case REDDIT_CONSTANT.UPWARD:
        // remove existing upvote
        if(comment.upvotedByCurrentUser) {
          payload = this.removeExistingUpvote(comment, mockReq);
          return;
        }
        // switch vote
         else if(comment.downVotedByCurrentUser) {
          payload = this.switchExistingUpvote(comment, mockReq);
         }
         // new vote
          else {
            comment.totalUpvoteCount!++;
         }
        comment.upvotedByCurrentUser = true;
        comment.downVotedByCurrentUser = false;
        break;
        
      case REDDIT_CONSTANT.DOWNWORD:
        // remove existing downvote
        if(comment.downVotedByCurrentUser) {
          return this.removeExistingDownvote(comment);
        };
        // new downvote
        if (!comment.downVotedByCurrentUser) {
          comment.totalDownVoteCount! += 1; // new downvote count 
          
        // switch to downvote to upvote 
        } else if (comment.upvotedByCurrentUser) {
          comment.totalDownVoteCount! += 1; // Switch from upvote to downvote
          comment.totalUpvoteCount! -= 1;
        }
        comment.downVotedByCurrentUser = true;
        comment.upvotedByCurrentUser = false;
        break;
        
      default:
        break;
    }
    //this.commentEngagementService.performEngagementOpsOnComments(this.commentService.)
  }

  removeExistingEngagement(comment: Comments) {
    if(comment.upvotedByCurrentUser) {
      comment.totalUpvoteCount!--;
    }
  }

  switchExistingUpvote(comment: Comments, mockReq: Comments): CommentEngagement {
    comment.totalDownVoteCount!--;
    comment.totalUpvoteCount!++;
    return this.commentSharedService.createCommentEngagementPayload(mockReq,false,true,false);
}


  private removeExistingUpvote(comment: Comments, mockReq: Comments) {
    comment.upvotedByCurrentUser = false;
    comment.totalUpvoteCount! -=1 ; // reduce count
    return this.commentSharedService.createCommentEngagementPayload(mockReq,true,false,false);
  }

  private removeExistingDownvote(comment: Comments) {
    comment.downVotedByCurrentUser = false;
    comment.totalDownVoteCount! -=1 ; // reduce count
    return;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
