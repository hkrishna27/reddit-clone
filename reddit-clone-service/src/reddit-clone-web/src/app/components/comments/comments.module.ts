import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RedditCommentsComponent } from './reddit-comments/reddit-comments.component';
import { MaterialModule } from 'src/app/material/material.module';
import { RedditCommentReplyEditorComponent } from './reddit-comment-reply-editor/reddit-comment-reply-editor.component';
import { NgxEditorModule } from 'ngx-editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { MarkdownModule } from 'ngx-markdown';



@NgModule({
  declarations: [
    RedditCommentsComponent,
    RedditCommentReplyEditorComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    PickerModule,
    NgxEditorModule,
    FormsModule,
    ReactiveFormsModule,
    MarkdownModule.forRoot()
  ],
  exports: [
    RedditCommentsComponent,
    RedditCommentReplyEditorComponent
  ]
})
export class CommentsModule { }
