import { Component, ElementRef, EventEmitter, HostListener, OnInit, Output, Renderer2 } from '@angular/core';
import { Editor, Toolbar } from 'ngx-editor';

@Component({
  selector: 'app-reddit-comment-reply-editor',
  templateUrl: './reddit-comment-reply-editor.component.html',
  styleUrls: ['./reddit-comment-reply-editor.component.scss']
})
export class RedditCommentReplyEditorComponent implements OnInit {
  constructor(
    private renderer: Renderer2,
    private el: ElementRef
  ) { }

  @Output() showReplyEditor: EventEmitter<boolean> = new EventEmitter(true);
  @Output() emitComment: EventEmitter<string> = new EventEmitter();
  showEmojiEditor: boolean = false;
  editorMessage: string = '';
  editor!: Editor;
  toolbar: Toolbar = [
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['link'],
  ];

  // close the containers when clicking outside
  @HostListener('document:click', ['$event'])
  clickout(event: MouseEvent) {
    if (this.showEmojiEditor && !this.el.nativeElement.contains(event.target)) {
      this.showEmojiEditor = false;
    }
  }

  ngOnInit(): void {
    this.editor = new Editor();
  }

  /**
   * emits an event to close reply editor
   */
  closeReplyEditor() {
    this.showReplyEditor.emit(false);
  }

  /**
   * adds emoji
   * @param event emoji event
   */
  addEmoji(event: any) {
    const { editorMessage } = this;
    let text:string;
    if (editorMessage === '')  {
      text = event.emoji.native;
    } else {
      text = editorMessage + event.emoji.native;
    }
    // removes <p> and </p> tags from emoji
    const sanitizedContent = text.replace(/<\/?p>/g, ' ');
    this.editorMessage = sanitizedContent;
  }
 
  /**
   * adds the comment
   */
  addComment() {
    this.emitComment.emit(this.editorMessage);
  }

  // make sure to destory the editor
  ngOnDestroy(): void {
    this.editor.destroy();
  }
}
