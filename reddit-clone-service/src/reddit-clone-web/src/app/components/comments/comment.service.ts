import { Injectable } from '@angular/core';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';
import { Comments } from '../posts/model/post.model';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';
import { CommentEngagement } from 'generated/angular-client';

@Injectable({
  providedIn: 'root'
})
export class CommentSharedService {

  constructor(
    private authService: AuthWebService
  ) { }

  createCommentEngagementPayload(
    comment: Comments,
    removeExistingEngagement: boolean,
    switchExistingEngagement: boolean,
    addNewEngagement: boolean
  ): CommentEngagement {
    return {
      commentId: comment.id,
      upVoted: comment.upvotedByCurrentUser,
      downVoted: comment.downVotedByCurrentUser,
      removeExistingEngagement,
      switchExistingEngagement,
      addNewEngagement
    }
  }
  
  createCommentResponseModel(content: string, comment: Comments, savedCommmentId: number): Comments {
    // expand the replies of parent comment
    comment.isExpanded = true;
    return {
      id: savedCommmentId,
      userResponse: this.authService.getUserResponse(),
      parentCommentId: comment.id,
      createdBy: this.authService.getUserResponse().userEmail?.split('@')[0],
      updatedAt: new Date().toUTCString(),
      postResponseId: comment.postResponseId,
      content
    }
  }
}
