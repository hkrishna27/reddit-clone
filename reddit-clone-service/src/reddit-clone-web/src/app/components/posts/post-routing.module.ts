import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedditPostDetailsComponent } from './reddit-post-details/reddit-post-details.component';
import { UploadMultimediaComponent } from './upload-multimedia/upload-multimedia.component';

const routes: Routes = [
    { path: ':communityId/:postId', component: RedditPostDetailsComponent },
    { path: 'multi', component: UploadMultimediaComponent },
  ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }