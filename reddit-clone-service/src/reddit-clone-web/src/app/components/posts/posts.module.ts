import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RedditPostsComponent } from './reddit-posts/reddit-posts.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material/material.module';
import { RedditPostDetailsComponent } from './reddit-post-details/reddit-post-details.component';
import { CommentsModule } from "../comments/comments.module";
import { CreatePostsComponent } from './create-posts/create-posts.component';
import { PostBodyEditorComponent } from './post-body-editor/post-body-editor.component';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { NgxEditorModule } from 'ngx-editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from 'src/app/core/core.module';
import { MarkdownModule } from 'ngx-markdown';
import { PostsRoutingModule } from './post-routing.module';
import { UploadMultimediaComponent } from './upload-multimedia/upload-multimedia.component';
import { PostDetailsRightSidebarComponent } from './post-details-right-sidebar/post-details-right-sidebar.component';

@NgModule({
    declarations: [
        RedditPostsComponent,
        RedditPostDetailsComponent,
        CreatePostsComponent,
        PostBodyEditorComponent,
        UploadMultimediaComponent,
        PostDetailsRightSidebarComponent
    ],
    exports: [
        RedditPostsComponent
    ],
    imports: [
        PostsRoutingModule,
        CommonModule,
        SharedModule,
        MaterialModule,
        CommentsModule,
        PickerModule,
        NgxEditorModule,
        FormsModule,
        ReactiveFormsModule,
        CoreModule,
        MarkdownModule.forRoot(),
    ]
})
export class PostsModule { }
