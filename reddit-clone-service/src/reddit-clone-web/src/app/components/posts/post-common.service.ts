import { Injectable } from '@angular/core';
import { PostEngagementControllerService, PostResponse } from 'generated/angular-client';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';

@Injectable({
  providedIn: 'root'
})
export class PostCommonService {

  constructor(
    private postEngagementController: PostEngagementControllerService
  ) { }

  public onPostUpwardAndDownOperation(value: REDDIT_CONSTANT, post: PostResponse) {
    this.handleVotes(value, post);
  }

  private handleVotes(value: REDDIT_CONSTANT, post: PostResponse) {
    switch (value) {
      case REDDIT_CONSTANT.UPWARD:
        // remove existing upvote
        if(post.currentUserUpVoted) {
          return this.removeExistingUpvote(post);
        }
        // switch to upvote from downvote
        else if(post.currentUserDownVoted) {
          post.upVotesCount! += 1; 
          post.downVotesCount! -= 1;
        } else {
          post.upVotesCount! += 1; // new upvote count
        }
        post.currentUserUpVoted = true;
        post.currentUserDownVoted = false;
        break;
        
      case REDDIT_CONSTANT.DOWNWORD:
        // remove existing downvote
        if(post.currentUserDownVoted) {
          return this.removeExistingDownvote(post);
        }  
        // Switch from upvote to downvote
        else if (post.currentUserUpVoted) {
          post.downVotesCount! += 1; 
          post.upVotesCount! -= 1;
        }
        // new downvote
        else {
          post.downVotesCount! += 1; // new downvote count 
        // switch to downvote to upvote 
        }
        post.currentUserDownVoted = true;
        post.currentUserUpVoted = false;
        break;
        
      default:
        break;
    }
    // call api to udate or create new engagement
    this.postEngagementController.upvoteOrDownVotePost(
      {
        id: post.currentUserPostEngagementId,
        postId: post.id!, 
        communityId: post.communityId!, 
        upVoted: value === REDDIT_CONSTANT.UPWARD ? true: false, 
        downVoted: value === REDDIT_CONSTANT.DOWNWORD ? true: false
      }
  ).subscribe({
    next: (data)=> post.currentUserPostEngagementId = data.id,
    error: ()=> console.error("could not perform operation")
  });
  }

  private deleteEngagementOfUser(postEngagementId: number) {
    this.postEngagementController.deleteEngagementWithPost(postEngagementId).subscribe(
      {
        error: () => console.error("could not perform operation")
      }
    );
  }

  private removeExistingUpvote(post: PostResponse) {
    post.currentUserUpVoted = false;
    post.upVotesCount! -=1 ; // reduce count
    this.deleteEngagementOfUser(post.currentUserPostEngagementId!)
    return;
  }

  private removeExistingDownvote(post: PostResponse) {
    post.currentUserDownVoted = false;
          post.downVotesCount! -=1 ;
          this.deleteEngagementOfUser(post.currentUserPostEngagementId!);
          return;
  }
}
