import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommunityResponse, DataResponseMessageResponse, PostControllerService, PostRequest, S3ControllerService } from 'generated/angular-client';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';
import { OverlayService } from 'src/app/shared/services/overlay.service';
import { S3UtilServiceService } from 'src/app/shared/services/s3-util-service.service';
import { HttpClient } from '@angular/common/http';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';

@Component({
  selector: 'app-create-posts',
  templateUrl: './create-posts.component.html',
  styleUrls: ['./create-posts.component.scss']
})
export class CreatePostsComponent implements OnInit {

  createPostForm!: FormGroup;
  currentUser!: any;
  progress: { [key: string]: number } = {};
  filesToBeUploaded: Array<File> = [];
  uploading = false;

  constructor(
    private fb: FormBuilder,
    private postService: PostControllerService,
    private authWebService: AuthWebService,
    private router: Router,
    private overlay: OverlayService,
    private s3Backend: S3ControllerService,
    private s3Frontend: S3UtilServiceService,
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
    this.createPostForm = this.initializeCreatePostForm();
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.authWebService.currentUserObservable.subscribe(( data =>
      this.currentUser = data
    ));
  }

  initializeCreatePostForm() {
    return this.fb.group({
      communityId: new FormControl(null, Validators.required),
      title: new FormControl(null , Validators.required),
      body: new FormControl(null),
      images: new FormArray([]),
      video: new FormControl(null),
      link: new FormControl(null),
      postType: new FormControl('TEXT')
    })
  }

  emittedTextBody(event: string) {
    this.createPostForm.get('body')?.patchValue(event);
  }

  emittedSelectedCommunity(community: CommunityResponse) {
    this.createPostForm.get('communityId')?.patchValue(community.id);
  }

  createPost() {
    this.uploading = true;
    let payload: PostRequest = {
      communityId: this.createPostForm?.value?.communityId,
      userId: this.currentUser?.id,
      postType: PostRequest.PostTypeEnum.Text,
      textTitle: this.createPostForm?.value?.title
    }
    switch(this.createPostForm.value?.postType) {
      case "TEXT":
        payload.postType = PostRequest.PostTypeEnum.Text
        this.createTextPayload(payload);
        break;
      case "MULTIMEDIA":
        payload.postType = PostRequest.PostTypeEnum.ImageAndVideo
        this.uploadFilesToS3(payload);
        break;
      case "LINK":
        this.createLinkPayload(payload);
        break;
    }
  }

  callCreatePostApi(payload: PostRequest) {
    this.postService.createRedditPost(payload).subscribe(
      (data: DataResponseMessageResponse) => {
        console.log(data);
        this.uploading = false;
        this.overlay.closeOverlay();
        this.router.navigate(['/post', payload.communityId , data.data?.id ]);
      }
    )
  }

  uploadFilesToS3(payload: PostRequest) {
    const videoFile = this.createPostForm?.value?.video
    const imageFiles = this.createPostForm?.value?.images as Array<File>
    if(videoFile) {
      this.filesToBeUploaded.push(videoFile);
    } else if(imageFiles.length>0) {
      this.filesToBeUploaded.push(...imageFiles)
    }
    if(this.filesToBeUploaded.length>0) {
      this.s3Frontend.uploadFilesToS3WithSignedUrls(this.filesToBeUploaded,REDDIT_CONSTANT.VIDEO_S3).subscribe({
        next: (_data) => {
          this.createMultimediaPayload(payload);
        },
        error: (_error) => {
          this.uploading = false;
          this.filesToBeUploaded = [];
        }
      })
    }
  }

  getProgressOfFile(fileName:string): number {
    return this.s3Frontend.getProgressOfFile(fileName);
  }

  createTextPayload(payload: PostRequest) {
    payload.textBody = this.createPostForm.value?.body;
    this.callCreatePostApi(payload);
  }

  createMultimediaPayload(payload: PostRequest) {
    const urlMaps = this.s3Frontend.getUrlMapsOfSignedUrl();
    payload.imageUrls = [];
    payload.videoUrls = [];
  
    this.filesToBeUploaded.forEach((file) => {
      const urlPath = { urlPath: this.s3Frontend.splitImageUrlFromPreSignedUrl(urlMaps[file.name]!) };
      if (file.type.startsWith('video')) {
        payload.videoUrls!.push(urlPath);
      } else {
        payload.imageUrls!.push(urlPath);
      }
    });
  
    this.callCreatePostApi(payload);
  }

  createLinkPayload(payload: PostRequest) {
    payload.link = this.createPostForm.value?.link;
    return payload;
  }
}
