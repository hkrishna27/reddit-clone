import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { FormGroup } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';
import { S3UtilServiceService } from 'src/app/shared/services/s3-util-service.service';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';

@Component({
  selector: 'app-upload-multimedia',
  templateUrl: './upload-multimedia.component.html',
  styleUrls: ['./upload-multimedia.component.scss']
})
export class UploadMultimediaComponent implements OnInit {

  @Input() createPostForm!: FormGroup;
  currentIndex: number = 0;
  multipleVideos = false;
  imagesArray: { file: File, url: SafeUrl }[] = [];
  videoFile: { file: File, url: SafeUrl } | null = null;

  constructor(
    private sanitizer: DomSanitizer,
    private alertService: AlertService,
    public s3Frontend: S3UtilServiceService
  ) {}
  ngOnInit(): void {}

  onFileSelected(event: any) {
    const files = event.target.files;
    this.validateAndPatchData(files);
  }

  validateAndPatchData(files: File[]) {
    for (let i = 0; i < files.length; i++) {
      if (files[i].type.startsWith('image')) {
        this.imagesArray.push({ file: files[i], url: this.sanitizeURL(URL.createObjectURL(files[i])) });
      } else if (this.videoFile === null) {
        this.videoFile = { file: files[i], url: this.sanitizeURL(URL.createObjectURL(files[i])) };
      } else {
        this.multipleVideos = true;
      }
    }

    if (this.imagesArray.length > 0 && this.videoFile) {
      this.showAlertMessage(REDDIT_CONSTANT.BOTH_IMAGE_AND_VIDEO);
    } else if (this.multipleVideos) {
      this.showAlertMessage(REDDIT_CONSTANT.MULTIPLE_VIDEO);
    } else {
      this.patchDataToForm();
    }
  }

  patchDataToForm() {
    this.createPostForm.get('images')?.value.push(...this.imagesArray.map(item => item.file));
    this.createPostForm.get('video')?.patchValue(this.videoFile?.file || null);
  }

  showAlertMessage(message: REDDIT_CONSTANT) {
    this.alertService.showAlert({
      type: 'error',
      message,
      componentId: 'app-level'
    });
    this.imagesArray = [];
    this.videoFile = null;
    this.multipleVideos = false;
  }

  onFileDrop(event: any) {
    event.preventDefault();
    const files = event.dataTransfer.files;
    this.validateAndPatchData(files);
  }

  allowDrop(event: any) {
    event.preventDefault();
  }

  previousFile() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
  }

  nextFile() {
    if (this.currentIndex < this.imagesArray.length - 1) {
      this.currentIndex++;
    }
  }

  removeFile(fileToBeDeleted: { file: File, url: SafeUrl }) {
    if (fileToBeDeleted.file.type.startsWith('image')) {
      this.imagesArray = this.imagesArray.filter(item => item.file.name !== fileToBeDeleted.file.name);
    } else {
      this.videoFile = null;
    }
    this.patchDataToForm();
  }

  get currentFile() {
    return this.imagesArray[this.currentIndex] || null;
  }

  sanitizeURL(url: string): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}