import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  CommentControllerService,
  CommentRequest,
  CommunityControllerService,
  CommunityResponse,
  PostControllerService,
  PostResponse,
  UrlRequest,
} from 'generated/angular-client';
import { forkJoin, map, Observable, Subject, takeUntil } from 'rxjs';
import { PostCommonService } from '../post-common.service';
import { CommentsChangeDetecterService } from 'src/app/change-detecters/comments-change-detecter.service';
import { Comments } from '../model/post.model';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';
import { SharedService } from 'src/app/shared/services/shared.service';

@Component({
  selector: 'app-reddit-post-details',
  templateUrl: './reddit-post-details.component.html',
  styleUrls: ['./reddit-post-details.component.scss'],
})
export class RedditPostDetailsComponent implements OnInit {
  postDetailsData$!: Observable<{
    communityData: CommunityResponse;
    postData: PostResponse;
  }>;
  private unsubscribe$ = new Subject<void>();
  comments$!: Observable<Comments[]>;
  showCommentBox!: boolean;
  currentIndex: number = 0;
  isImageRendering: boolean = true;
  upward: REDDIT_CONSTANT = REDDIT_CONSTANT.UPWARD;
  downward: REDDIT_CONSTANT = REDDIT_CONSTANT.DOWNWORD;

  // paging
  currentPage!: number;
  totalElements!: number;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private communityController: CommunityControllerService,
    private postController: PostControllerService,
    public sharedService: SharedService,
    private postCommonService: PostCommonService,
    private commentService: CommentControllerService,
    private commentsChangeDetector: CommentsChangeDetecterService,
    private authWebService: AuthWebService
  ) {}

  ngOnInit(): void {
    /** change detectors */
    this.detectCommentsChange();
    this.route.params.subscribe((params) => {
      this.fetchPostAndCommunityDetails(params['communityId'], params['postId']);
      this.fetchCommentsForPost(params['postId']);
    });
  }

  detectCommentsChange() {
    this.comments$ = this.commentsChangeDetector.commentsData$.pipe(
      map(comments => {
        this.totalElements = comments.totalElements!;
        return (comments?.content || []);
      })
    );
  }

  onPostUpwardAndDownOperation(value: REDDIT_CONSTANT, post: PostResponse) {
    this.postCommonService.onPostUpwardAndDownOperation(value,post);
  }

  fetchPostAndCommunityDetails(communityId:number , postId: number) {
    const postData$ = this.postController.getPostById(postId, communityId);
    const communityData$ = this.communityController.fetchCommunityById(communityId);

    this.postDetailsData$ = forkJoin([postData$, communityData$]).pipe(
      takeUntil(this.unsubscribe$),
      map(([postData, communityData]) => {
        return { postData, communityData };
      })
    );
  }

  fetchCommentsForPost(postId: number) {
     const paginationPayload = Object.assign({}, this.sharedService.getPageableRequest(), {sortBy: 'createdAt', direction: REDDIT_CONSTANT.DIRECTION_DESC})
     console.log(paginationPayload);
     this.commentService.getCommentsByPostId(postId, paginationPayload).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(commentsData => {
        this.commentsChangeDetector.setCommentsData(commentsData);
    });
  }

    /** emmited event from editor to show reply box or not */
    emittedShowReplyEditorEvent(showReplyEditor: boolean) {
      this.showCommentBox = showReplyEditor;
    }
  
    /**
     * catches comment event
     * @param comment emmited comment from reply box
     */
    emittedComment(comment: string, post:PostResponse) {
      console.log(this.authWebService.getCurrentUserValue())
      const payload: CommentRequest = {
        postId: post?.id as number,
        userId: this.authWebService.getCurrentUserValue().id,
        content: comment
      }
     this.commentService.addComment(payload).pipe(
      takeUntil(this.unsubscribe$)
     ).subscribe(_success => {
      this.showCommentBox = false;
      this.fetchCommentsForPost(post.id!);
    });
    }

  backToCommunityPage(communityId: number) {
    this.router.navigate(['/community', communityId]);
  }

  previousFile() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
  }

  currentFile(images: UrlRequest[]) {
    return images[this.currentIndex] || null;
  }

  nextFile(images: UrlRequest[]) {
    if (this.currentIndex < images.length - 1) {
      this.currentIndex++;
    }
  }

  onImageLoad() {
    this.isImageRendering = false;
  }

  onImageError() {
    this.isImageRendering = false;
    // Optionally handle the error case
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
