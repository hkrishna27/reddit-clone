import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { PostControllerService, PostResponse, UrlRequest } from 'generated/angular-client';
import { map, Observable, Subject, takeUntil } from 'rxjs';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';
import { PostsChangeDetecterService } from 'src/app/change-detecters/posts-change-detecter.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { PostCommonService } from '../post-common.service';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';
import { SharedService } from 'src/app/shared/services/shared.service';
import { PostCatagory } from 'src/app/shared/enums/catagory.enum';
import { CatagoryChangeDetecterService } from 'src/app/change-detecters/catagory-change-detector.service';

@Component({
  selector: 'app-reddit-posts',
  templateUrl: './reddit-posts.component.html',
  styleUrls: ['./reddit-posts.component.scss']
})
export class RedditPostsComponent implements OnInit {
  @Input() postCategory!: string | null;
  posts$!: Observable<PostResponse[]>;
  private readonly unsubscribe$ = new Subject<void>();
  currentIndex: number = 0;
  isImageRendering: boolean = true;
  upward: REDDIT_CONSTANT = REDDIT_CONSTANT.UPWARD;
  downward: REDDIT_CONSTANT = REDDIT_CONSTANT.DOWNWORD;


  constructor(
    private readonly commuCd: CommunityChangeDetecterService,
    private readonly postCd: PostsChangeDetecterService,
    private readonly catagoryCd: CatagoryChangeDetecterService,
    private readonly postController: PostControllerService,
    public sharedService: SharedService,
    private readonly alertService: AlertService,
    private readonly router: Router,
    private readonly postCommonService: PostCommonService
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['postCategory']) {
      console.log(`Category changed to: ${this.postCategory}`);
      if(this.postCategory) {
        this.fetchPostsByCatagoryType(this.postCategory);
      }
    }
  }

  ngOnInit(): void {
    this.onCommuityIdChange();
    this.onCatagoryChange();
    this.postsDataChange();
  }

  onCatagoryChange() {
    this.catagoryCd.catagoryData$.pipe(
      takeUntil(this.unsubscribe$)
    )
      .subscribe(catagory => {
        this.fetchPostsByCatagory(catagory);
      })
  }

  onCommuityIdChange() {
    this.commuCd.communityId$.pipe(
      takeUntil(this.unsubscribe$)
    )
      .subscribe(communityId => {
        if (!isNaN(communityId)) {
          this.fetchPostsByCommunityId(communityId);
        }
      })
  }

  postsDataChange() {
    this.posts$ = this.postCd.postData$.pipe(
      map((data) => data?.content || [])
    )
  }

  onPostUpwardAndDownOperation(value: REDDIT_CONSTANT, post: PostResponse) {
    this.postCommonService.onPostUpwardAndDownOperation(value, post);
  }

  fetchPostsByCommunityId(communityId?: number) {
    if (communityId !== undefined) {
      this.postController.getPostsByCommunityId(communityId, this.sharedService.getPageableRequest())
        .subscribe({
          next: (data) => {
            ;
            this.postCd.setPostData(data);
          },
          error: (_error) => {
            this.alertService.showAlert({
              type: 'error',
              message: REDDIT_CONSTANT.ERROR,
              componentId: 'app-level'
            });
          }
        });
    }
  }

  fetchPostsByCatagory(catagory: PostCatagory) {
    if (catagory !== undefined) {
      this.postController.getPostsByCategory(catagory, this.sharedService.getPageableRequest())
        .subscribe({
          next: (data) => {
            ;
            this.postCd.setPostData(data);
          },
          error: (_error) => {
            this.alertService.showAlert({
              type: 'error',
              message: REDDIT_CONSTANT.ERROR,
              componentId: 'app-level'
            });
          }
        });
    }
  }

  fetchPostsByCatagoryType(catagoryType: string) {
    this.postController.getPostsByCategoryType(catagoryType.substring(1).split('-').join(' '),this.sharedService.getPageableRequest()).subscribe({
      next: (data) => {
        ;
        this.postCd.setPostData(data);
      },
      error: (_error) => {
        this.alertService.showAlert({
          type: 'error',
          message: REDDIT_CONSTANT.ERROR,
          componentId: 'app-level'
        });
      }
    });
  }


  openPostOnNewComponent(post: PostResponse) {
    this.router.navigate(['/post', post.communityId, post.id]);
  }

  previousFile() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
  }

  currentFile(images: UrlRequest[]) {
    return images[this.currentIndex] || null;
  }

  nextFile(images: UrlRequest[]) {
    if (this.currentIndex < images.length - 1) {
      this.currentIndex++;
    }
  }

  onImageLoad() {
    this.isImageRendering = false;
  }

  onImageError() {
    // Optionally handle the error case
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
