import { Component, ElementRef, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { Editor, Toolbar } from 'ngx-editor';

@Component({
  selector: 'app-post-body-editor',
  templateUrl: './post-body-editor.component.html',
  styleUrls: ['./post-body-editor.component.scss'],
})
export class PostBodyEditorComponent implements OnInit {
  @Output() emitBody: EventEmitter<string> = new EventEmitter();
  editorMessage: string = '';
  showEmojiEditor!: boolean;
  editor!: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ];

  constructor(
    private el: ElementRef
  ) {}

  ngOnInit(): void {
    this.editor = new Editor();
  }
  /**
   * adds emoji
   * @param event emoji event
   */
  addEmoji(event: any) {
    const { editorMessage } = this;
    let text: string;
    if (editorMessage === '') {
      text = event.emoji.native;
    } else {
      text = editorMessage + event.emoji.native;
    }
    // removes <p> and </p> tags from emoji
    const sanitizedContent = text.replace(/<\/?p>/g, ' ');
    this.editorMessage = sanitizedContent;
    this.emitBodyText();
  }

  /**
   * adds the post
   */
  emitBodyText() {
    this.emitBody.emit(this.editorMessage);
  }

  // close the containers when clicking outside
  @HostListener('document:click', ['$event'])
  clickout(event: MouseEvent) {
    if (this.showEmojiEditor && !this.el.nativeElement.contains(event.target)) {
      this.showEmojiEditor = false;
    }
  }

  /**
   * emits an event to close reply editor
   */
  closeReplyEditor() {}
}
