import { UserResponse } from "generated/angular-client";

export interface Comments { 
    id?: number;
    parentCommentId?: number;
    createdBy?: string;
    updatedAt?: string;
    content?: string;
    userResponse?: UserResponse;
    postResponseId?: number;
    totalUpvoteCount?: number;
    totalDownVoteCount?: number;
    upvotedByCurrentUser?: boolean;
    downVotedByCurrentUser?: boolean;
    isExpanded?: boolean;
    replies?: Comments[]
}

export interface FileWithType {
    file: File
    type: string
}
