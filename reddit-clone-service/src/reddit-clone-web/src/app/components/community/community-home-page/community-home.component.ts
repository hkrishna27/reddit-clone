import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';

@Component({
  selector: 'app-community-home',
  templateUrl: './community-home.component.html',
  styleUrls: ['./community-home.component.scss']
})
export class CommunityHomeComponent implements OnInit {
  private unsubscribe$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private cd: CommunityChangeDetecterService
  ) { }

  ngOnInit(): void {
    this.route.params
    .pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(params => {
      this.cd.setCommunityId(params['id']);
    })
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
