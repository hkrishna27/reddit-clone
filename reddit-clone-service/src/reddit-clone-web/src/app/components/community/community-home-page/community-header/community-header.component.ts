import { Component, ElementRef, OnInit } from '@angular/core';
import {
  CommunityControllerService,
  CommunityResponse,
} from 'generated/angular-client';
import { Observable, Subject, takeUntil } from 'rxjs';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';
import { CreatePostsComponent } from 'src/app/components/posts/create-posts/create-posts.component';
import { CommunityEnums } from 'src/app/shared/enums/community.enum';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';
import { OverlayService } from 'src/app/shared/services/overlay.service';

@Component({
  selector: 'app-community-header',
  templateUrl: './community-header.component.html',
  styleUrls: ['./community-header.component.scss'],
})
export class CommunityHeaderComponent implements OnInit {
  selectedValue!: string;
  singleCommunityData$: Observable<CommunityResponse> = new Observable();
  defaultImage: string = 'assets/images/reddit-tab-icon.png';
  private unsubscribe$ = new Subject<void>();

  filterDropdownValues = [
    { value: 'hot', viewValue: 'hot' }, // Default option
    { value: 'new', viewValue: 'new' },
    { value: 'top', viewValue: 'top' },
    { value: 'rising', viewValue: 'rising' },
  ];

  constructor(
    private commuCd: CommunityChangeDetecterService,
    private communityController: CommunityControllerService,
    private authWebService: AuthWebService,
    private overlay: OverlayService,
    private eleRef: ElementRef
  ) { }

  ngOnInit(): void {
    this.commuCd.communityId$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((communityId) => {
        this.communityController.fetchCommunityById(communityId)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(communityData => {
            this.commuCd.setSingleCommunityData(communityData);
            this.singleCommunityData$ = this.commuCd.singleCommunityData$;
          });
      });
  }

  createPost() {
    this.overlay.closeOverlay();
    this.overlay.openOverlay(CreatePostsComponent, true, this.eleRef, 800);
  }

  joinOrUnjoinCommunity(community: CommunityResponse) {
    let operation: CommunityEnums;
    if (community.loggedUserMember) {
      operation = CommunityEnums.UNJOIN;
      delete community.loggedUserMember;
      community.totalCommunityMembers! -= 1;
    } else {
      operation = CommunityEnums.JOIN;
      community.loggedUserMember = this.authWebService.getUserResponse();
      community.totalCommunityMembers! += 1;
    }
    this.communityController.joinCommunity(
      this.authWebService.getUserResponse().userId!,
      community.id!,
      operation
    ).subscribe();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
