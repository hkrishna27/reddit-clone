import { Component } from '@angular/core';
import { CommunityResponse } from 'generated/angular-client';
import { Observable, Subject } from 'rxjs';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';

@Component({
  selector: 'app-community-right-sidebar',
  templateUrl: './community-right-sidebar.component.html',
  styleUrls: ['./community-right-sidebar.component.scss'],
})
export class CommunityRightSidebarComponent {
  private unsubscribe$ = new Subject<void>();
  singleCommunityData$! : Observable<CommunityResponse>;
  currentUser: any;

  constructor(
    private communityChangeDetecterService: CommunityChangeDetecterService,
    private authWebService: AuthWebService
  ) {}

  ngOnInit(): void {
    this.singleCommunityData$ = this.communityChangeDetecterService.singleCommunityData$;
    this.currentUser = this.authWebService.getCurrentUserValue();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
