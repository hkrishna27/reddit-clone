import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommunityHomeComponent } from './community-home-page/community-home.component';

const routes: Routes = [
    { path: ':id', component: CommunityHomeComponent },
  ];
  
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunityRoutingModule { }