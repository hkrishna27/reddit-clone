import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterCommunityComponent } from './register-community/register-community.component';
import { MaterialModule } from 'src/app/material/material.module';
import { CommunityDescriptionStepComponent } from './register-community/first-step/community-description-step.component';
import { TopicsStepComponent } from './register-community/second-step/topics-step.component';
import { AccessibilityStepComponent } from './register-community/third-step/accessibility-step.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommunityHomeComponent } from './community-home-page/community-home.component';
import { CommunityHeaderComponent } from './community-home-page/community-header/community-header.component';
import { CommunityRightSidebarComponent } from './community-home-page/community-right-sidebar/community-right-sidebar.component';
import { PostsModule } from '../posts/posts.module';
import { CommunityRoutingModule } from './community-routing.module';

@NgModule({
  declarations: [
    RegisterCommunityComponent,
    CommunityDescriptionStepComponent,
    TopicsStepComponent,
    AccessibilityStepComponent,
    CommunityHomeComponent,
    CommunityHeaderComponent,
    CommunityRightSidebarComponent,
  ],
  imports: [
    CommunityRoutingModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PostsModule
  ],
  exports: [
  ]
})
export class CommunityModule { }
