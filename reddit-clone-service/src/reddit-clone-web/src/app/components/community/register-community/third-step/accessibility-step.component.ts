import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CommunityControllerService, CommunityRequest, S3ControllerService, SignedUrlRequestS3 } from 'generated/angular-client';
import { BehaviorSubject, catchError, map, of, switchMap } from 'rxjs';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';
import { AccessibityOptions } from 'src/app/shared/models/shared.model';
import { AlertService } from 'src/app/shared/services/alert.service';
import { OverlayService } from 'src/app/shared/services/overlay.service';
import { S3UtilServiceService } from 'src/app/shared/services/s3-util-service.service';

@Component({
  selector: 'accessibility-step',
  templateUrl: './accessibility-step.component.html',
})

export class AccessibilityStepComponent implements OnInit {

  @Input() communityStepperForm!: FormGroup;
  @Output() disableFormWhileUploadingEmitter = new EventEmitter<boolean>();
  disableFormWhileUploading: BehaviorSubject<boolean> = new BehaviorSubject(false);

  matRadioOptions: AccessibityOptions[] = [
    {
      'icon': 'public',
      'name': REDDIT_CONSTANT.PUBLIC,
      'description': 'Anyone can view and contribute',
      'checked': true
    },
    {
      'icon': 'visibility',
      'name': REDDIT_CONSTANT.RESTRICTED,
      'description': 'Anyone can view, but only approved users can contribute',
      'checked': false
    },
    {
      'icon': 'lock',
      'name': REDDIT_CONSTANT.PRIVATE,
      'description': 'Only approved users can view and contribute',
      'checked': false
    }
  ]

  constructor(
    private communityControllerService: CommunityControllerService,
    private s3Service: S3ControllerService,
    private s3Utils: S3UtilServiceService,
    private httpClient: HttpClient,
    private alertService: AlertService,
    private communityDataService: CommunityChangeDetecterService,
    private readonly overlayService: OverlayService,
  ) { }

  ngOnInit(): void {
  }

  accessibilityModification(accessibilityModification: string) {
    this.communityStepperForm.get('accessibility')?.patchValue(accessibilityModification);
  }

  createCommunity() {
    const file: File = this.communityStepperForm.controls['communityNameAndDescription'].get('imageS3')?.value;
    // if file is there upload it to s3
    this.disableFieldsWhileUploading(true);
    if (file) {
      const request: SignedUrlRequestS3 = { fileName: file.name, s3BucketFolder: REDDIT_CONSTANT.COMMUNITY_S3, httpMethod: SignedUrlRequestS3.HttpMethodEnum.Put };
      // call api to get pre-signed url
      this.s3Service.getSignedUrlS3(request).pipe(
        switchMap(s3Res => {
          // if pre signed url is threre then upload image
          if (s3Res?.signedUrl) {
            const headers = new HttpHeaders({
              'Content-Type': file.type
            });
            // call s3 client api to uplaod image
            return this.httpClient.put(s3Res.signedUrl, file, { headers }).pipe(
              map(_data => {
                // if successful write s3Image url and return observable
                return { success: true, message: REDDIT_CONSTANT.UPLOAD_SUCCESS , s3ImageUrl: s3Res.signedUrl };
              }),
              // if failed during upload then send error response as observable
              catchError((error: HttpErrorResponse) => {
                this.showAlertsIfErrorPopsUp();
                return of({ success: false, message: REDDIT_CONSTANT.UPLOAD_FAILED, s3ImageUrl: null });
              })
            );
          }
          // if pre-signed url is not there send a observable with the error
          return of({ success: false, message: REDDIT_CONSTANT.SIGNED_URL_FAILED, s3ImageUrl: null });
        }),
        // if pre-signed api fails then return error response as observable
        catchError(error => {
          this.showAlertsIfErrorPopsUp();
          return of({ success: false, message: REDDIT_CONSTANT.S3_UPLOAD_FAILED, s3ImageUrl: null });
        })
      ).subscribe(
        response => {
          // if upload is success generate payload
          if(response.success) {
            this.communityStepperForm.controls['s3ImageUrl']?.patchValue(this.s3Utils.splitImageUrlFromPreSignedUrl(response.s3ImageUrl!));
            // create community payload request and call api
           this.createCommunityApiCall(this.generateCommunityRequestPayload());
          }
        }, 
        error => {
          // if some error occured
          this.showAlertsIfErrorPopsUp();
        }
      );
    } else {
      this.createCommunityApiCall(this.generateCommunityRequestPayload());
    }
  }

  createCommunityApiCall(payload: CommunityRequest): void {
      this.communityControllerService.createCommunity(payload).subscribe(
        _data => {
          this.disableFieldsWhileUploading(false);
          // fetch latest data and call data service to update data
          // a change detecter will called wherever data is being used
          this.communityControllerService.fetchAllCommunities().subscribe(
            data => {
                this.communityDataService.setCommunityData(data);
                this.overlayService.closeOverlay();
              }
          );
        },
        error => {
          this.showAlertsIfErrorPopsUp();
        }
    ) 
  }
  
  generateCommunityRequestPayload(): CommunityRequest {
    let communityName = this.communityStepperForm?.value?.communityNameAndDescription?.name;
    if(communityName) {
      communityName = "r/" + communityName;
    }
    
    return {
      communityName: communityName,
      communityDescription: this.communityStepperForm?.value?.communityNameAndDescription.description,
      communityProfileImageUrl: this.communityStepperForm?.value?.s3ImageUrl,
      topics: this.communityStepperForm?.value?.topics,
      accessibility: this.communityStepperForm?.value.accessibility,
      adult: this.communityStepperForm?.value.isAdult 
    }
  }

  disableFieldsWhileUploading(diable: boolean) {
    if(diable) {
      this.communityStepperForm.get('isAdult')?.disable();
    } else {
      this.communityStepperForm.get('isAdult')?.enable();
    }
    this.disableFormWhileUploading.next(diable);
    this.disableFormWhileUploadingEmitter.emit(diable);
  }

  showAlertsIfErrorPopsUp():void {
    this.alertService.showAlert({type: 'error', message: REDDIT_CONSTANT.ERROR, componentId: "app-level"})
    // make sure button are enabled after errors are logged
    setTimeout(()=> {
      this.disableFieldsWhileUploading(false);
    },1000);
  }
}
