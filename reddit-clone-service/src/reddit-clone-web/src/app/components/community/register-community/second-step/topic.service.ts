import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface FrontEndChildTopics {
  name: string;
  selected: boolean;
}

export interface FrontEndTopics {
  name: string;
  selected: boolean;
  children: FrontEndChildTopics[];
}

@Injectable({
  providedIn: 'root'
})
export class TopicService {

  private topicsSubject = new BehaviorSubject<FrontEndTopics[]>([]);
  topics$ = this.topicsSubject.asObservable();

  constructor() {}

  setTopics(topics: FrontEndTopics[]) {
    this.topicsSubject.next(topics);
  }

  selectPrimaryTopic(value: string) {
    const updatedTopics = this.topicsSubject.value.map(topic => {
      if(topic.name === value) {
        topic.selected = true;
        topic.children.forEach(f=> f.selected=true)
      }
      return topic;
    });
    this.topicsSubject.next(updatedTopics);
  }

  removePrimaryTopic(value: string) {
    const updatedTopics = this.topicsSubject.value.map(topic => {
      if(topic.name === value) {
        topic.selected = false;
        topic.children.forEach(f=> f.selected=false)
      }
      return topic;
    });
    this.topicsSubject.next(updatedTopics);
  }

  selectChildTopic(value: string) {
      const updatedTopics = this.topicsSubject.value.map(topic => {
        topic.children.forEach(f=> {
        if(f.name === value) {
          f.selected = true;
        }
    })
      return topic;
    });
    this.topicsSubject.next(updatedTopics);
  }

  removeChildTopic(value: string) {
    const updatedTopics = this.topicsSubject.value.map(topic => {
      topic.children.forEach(f=> {
        if(f.name === value) {
          f.selected = false;
        }
      })
      return topic;
    });
    this.topicsSubject.next(updatedTopics);
  }
}
