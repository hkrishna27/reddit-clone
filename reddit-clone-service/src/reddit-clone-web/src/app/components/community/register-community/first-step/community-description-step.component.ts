import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CommunityControllerService } from 'generated/angular-client';
import { debounceTime } from 'rxjs';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';
import { setControlError } from 'src/app/shared/utils/form.utils';

@Component({
  selector: 'comm-description-step',
  templateUrl: './community-description-step.component.html',
})
export class CommunityDescriptionStepComponent implements OnInit {

  @ViewChild('fileInput') fileInput: any;
  @Input() communityStepperForm!: FormGroup;

  imageSrc: string | ArrayBuffer | null = null;

  constructor(
    private communityControllerService: CommunityControllerService
  ) { }

  ngOnInit(): void {
    this.checkIfCommunityIsAlreadyPresent();
  }

  checkIfCommunityIsAlreadyPresent() {
    this.communityStepperForm.get('communityNameAndDescription')?.get('name')!.valueChanges
      .pipe(debounceTime(500))
      .subscribe(
        (value: string) => {
          this.communityControllerService.existsByCommunityName(REDDIT_CONSTANT.SUBREDDIT_PREFIX + value)
            .subscribe(isExist => {
              if (isExist) {
                setControlError(this.communityStepperForm.get('communityNameAndDescription')?.get('name')!, { 'communityAlreayExist': '' })
              }
            });
        }
      );
  }

  onFileSelected(event: Event): void {
    const input = event.target as HTMLInputElement;
    if (input.files && input.files[0]) {
      const file = input.files[0];
      const reader = new FileReader();
      reader.onload = () => {
        this.imageSrc = reader.result;
        this.communityStepperForm.get('communityNameAndDescription')?.get('imageS3')?.patchValue(file);
      };
      reader.readAsDataURL(file);
    }
  }

  removeImage(): void {
    this.imageSrc = null;
    this.communityStepperForm.get('communityNameAndDescription')?.get('imageS3')?.patchValue(null);
    if (this.fileInput) {
      this.fileInput.nativeElement.value = '';
    }
  }

  appendSubredditPrefix(event: KeyboardEvent) {
    const communityName = this.communityStepperForm.get('communityNameAndDescription')?.get('name')!.value;
    if (event.key !== 'Backspace' && communityName && !communityName.startsWith('r/')) {
      this.communityStepperForm.get('communityNameAndDescription')?.get('name')!.setValue('r/' + communityName, { emitEvent: false });
    }
  }
}
