import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';

@Component({
  selector: 'app-register-community',
  templateUrl: './register-community.component.html',
  styleUrls: ['./register-community.component.scss']
})
export class RegisterCommunityComponent implements OnInit {

  communityStepperForm!: FormGroup;
  disableFieldsWhileUpload: boolean = false;

  constructor(
    private readonly fb:FormBuilder
  ) { }

  ngOnInit(): void {
    this.communityStepperForm = this.intializeCommunityStepperForm();
    }

  intializeCommunityStepperForm(): FormGroup {
      return this.fb.group({
      communityNameAndDescription: new FormGroup( {
        name: new FormControl(null, Validators.required),
        imageS3: new FormControl(null),
        description: new FormControl(null, Validators.required)
      }),
      topics: this.fb.array([]),
      s3ImageUrl: new FormControl(''), 
      accessibility: new FormControl(REDDIT_CONSTANT.PUBLIC, Validators.required),
      isAdult: new FormControl(false)
    });
  }

  get topicsFormArray(): FormArray {
    return this.communityStepperForm?.controls['topics'] as FormArray;
  }

  initializeTopicFromGroup(): FormGroup {
    return this.fb.group({
      'primaryTopic': new FormControl(null, [Validators.required])
    });
  }

  disableFieldsWhileUploading(disable: boolean) {
    this.disableFieldsWhileUpload = disable;
  }

}
