import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { map } from 'rxjs';
import { FrontEndChildTopics, TopicService, FrontEndTopics } from './topic.service';
import { DataResponseListTopics, TopicControllerService, Topics } from 'generated/angular-client';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';

@Component({
  selector: 'topics-step',
  templateUrl: './topics-step.component.html',
  styleUrls: ['./topics-step.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicsStepComponent implements OnInit {

  topics$ = this.topicServiceAngular.topics$;

  @Input() communityStepperForm!: FormGroup;
  loadingTopics: REDDIT_CONSTANT = REDDIT_CONSTANT.LOADING_TOPICS;

  constructor(
    private fb: FormBuilder,
    private topicServiceAngular: TopicService,
    private topicController: TopicControllerService,
  ) { }

  ngOnInit(): void {
    // fire an backend api to get topics
    this.topicController.getAllTopics().pipe(
      // mapping them to frontend topics
      map(
        (topicsData: DataResponseListTopics) => topicsData?.data?.map((topic : Topics) => ({
          name: topic.primaryTopicName,
          selected: false,
          children: topic?.secondaryTopicNames?.map(
            (child) => ({
              name: child,
              selected: false
            }) as FrontEndChildTopics
          )
        }) as FrontEndTopics)
      )
    )
    .subscribe(
      {
        next : (response : FrontEndTopics[] | undefined) => {
          if(response)  {
            // calling frontend service to set data in observables
            this.topicServiceAngular.setTopics(response);
          }
        },
        error: (error) => {
          // to do when api fails
        }
      }
    )
  }

  get topicsFormArray(): FormArray {
    return this.communityStepperForm?.controls['topics'] as FormArray;
  }

  selectPrimaryTopic(selectedValue: FrontEndTopics) {
    this.topicServiceAngular.selectPrimaryTopic(selectedValue.name);
  }

  selectSecondryTopic(selectedValue: FrontEndChildTopics) {
    this.topicServiceAngular.selectChildTopic(selectedValue.name);
  }

  removePrimaryTopic(selectedValue: FrontEndTopics) {
    this.topicServiceAngular.removePrimaryTopic(selectedValue.name);
  }

  removeSecondryTopic(selectedValue: FrontEndChildTopics) {
    this.topicServiceAngular.removeChildTopic(selectedValue.name);
  }

  patchSelectedTopics() {
    // get all the selected topics from topic$ and assign the in topics form array
    const selectedTopics: string[] = [];
    this.topics$.subscribe((topics: FrontEndTopics[]) => {
      topics.forEach((topic:FrontEndTopics)=> {
        if(topic.selected) {
          selectedTopics.push(topic.name);
        } 
        selectedTopics.push(...topic.children.filter(child => child.selected).map(child => child.name));
      })
    });
    selectedTopics.forEach(ele => this.topicsFormArray.push(new FormControl(ele)));
  }
}