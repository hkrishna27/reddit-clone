import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthControllerService } from 'generated/angular-client';
import { PasswordValidator } from './password.validator';
import { AlertService } from 'src/app/shared/services/alert.service';
import { OverlayService } from 'src/app/shared/services/overlay.service';
import { LogInComponent } from '../log-in/log-in.component';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers: [PasswordValidator]
})
export class SignupComponent implements OnInit {

	signupform: FormGroup;
	isloading:boolean = false;
	p_hide: boolean = true;
	cp_hide: boolean = true;

	constructor(
		private authService: AuthControllerService,
		private fb: FormBuilder,
		private alertService: AlertService,
		private overlayService: OverlayService,
		private elementRef: ElementRef
	) {
		this.signupform = this.getSignUpForm();
	}

  	ngOnInit(): void {}

	registerUser() {
		if(this.signupform.valid) {
			this.authService.registerUser(this.signupform.value)
				.subscribe({
					next: (res) => {
						this.isloading = true;
						this.alertService.showAlert({type:'success',message:'User registered successfully',componentId:'sign-up'});
						setTimeout(()=>this.initiateLogin(),1000);
					},
					error: (err) => {
						this.isloading = false;
						this.alertService.showAlert({ type: 'error', message: err.error.message , componentId: 'sign-up' });
						
					}
				});
		}
	}

  	initiateLogin() {
        // close existing overlay		
        this.overlayService.closeOverlay();
        this.overlayService.openOverlay(LogInComponent, true, this.elementRef, Number(REDDIT_CONSTANT.LOG_IN_OVERLAY_WIDTH))
  	}

  	getSignUpForm(): FormGroup {
		return this.fb.group (
			{
				username: [null, [Validators.required]],
				email: [null, [Validators.required, Validators.email]],
				password: [null, [Validators.required, Validators.minLength(10), , Validators.maxLength(20),
					Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$')]],
				confirmPassword: [null, [Validators.required]]
			}, {
				validators: PasswordValidator.validate(this)
			}
		)
  	}
}
