import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";
import { SignupComponent } from "./signup.component";
import { setControlError } from "src/app/shared/utils/form.utils";

export class PasswordValidator {

    static validate(ctx: SignupComponent): ValidatorFn 
        { 
            return (control:AbstractControl):ValidationErrors | null => {
                
                const passwordControl = ctx.signupform?.get('password');
                const confirmPaswordControl = ctx.signupform?.get('confirmPassword');
                
                if(passwordControl?.value !== confirmPaswordControl?.value) {
                    setControlError(confirmPaswordControl, {passwordMismatch:''});
                }
                return  null;

            }
        };

}