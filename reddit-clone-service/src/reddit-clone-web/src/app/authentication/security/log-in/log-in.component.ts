import { Component, ElementRef, OnInit } from '@angular/core';
import { SignupComponent } from '../signup/signup.component';
import {
  AuthControllerService,
  CommunityControllerService,
  GoogleSignInResponse,
} from 'generated/angular-client';
import { OverlayService } from 'src/app/shared/services/overlay.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';
import { REDDIT_CONSTANT } from 'src/app/shared/constants/shared.constant';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';
import { jwtDecode } from 'jwt-decode';
import { CatagoryChangeDetecterService } from 'src/app/change-detecters/catagory-change-detector.service';
import { HOME_CATAGORY } from 'src/app/shared/enums/catagory.enum';

declare const google: any;

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss'],
})
export class LogInComponent implements OnInit {
  loginConstant = REDDIT_CONSTANT;
  loginForm!: FormGroup;
  isloading: boolean = false;
  user: GoogleSignInResponse | null = null;
  GOOGLE_CLIENT_ID: string = '543139071128-5q5e6ul30hpd01536t92s65i1va2e5h3.apps.googleusercontent.com';

  constructor(
    private readonly authService: AuthControllerService,
    private readonly authWebService: AuthWebService,
    private readonly communityCdService: CommunityChangeDetecterService,
    private readonly communityService: CommunityControllerService,
    private readonly catagoryCd: CatagoryChangeDetecterService,
    private readonly fb: FormBuilder,
    private readonly alertService: AlertService,
    private readonly overlayService: OverlayService,
    private readonly elementRef: ElementRef
  ) {
    this.loginForm = this.getLoginForm();
  }

  ngOnInit(): void {
    // Google Identity Services initialization
    this.initiateGoogleSignIn();
  }

  private initiateGoogleSignIn() {
    google.accounts.id.initialize({
      client_id: this.GOOGLE_CLIENT_ID,
      callback: (response: any) => this.handleGoogleCredentialResponse(response),
    });
    google.accounts.id.renderButton(document.getElementById('google-button'), {
      theme: 'outline',
      size: 'large',
    });
  }

  handleGoogleCredentialResponse(response: any): void {
    const token = response.credential;
    const decodedUser: any = jwtDecode(token);
    this.user = {
      id: decodedUser.sub,
      name: decodedUser.name,
      email: decodedUser.email,
      photoUrl: decodedUser.picture,
      provider: 'GOOGLE',
      authToken: token,
      idToken: token,
      authorizationCode: '',
      response: {},
    };
    
    this.authService.authenticateUser({'username':'RANDOM','password':'RANDOM','googleSignInResponse': this.user}).subscribe({
      next: (response) => {
        this.handleSuccess(response);
      },
      error: (error) => {
        this.handleFailure(error);
      },
    });
  }

  initiateSignUp() {
    // close existing overlay
    this.overlayService.closeOverlay();
    this.overlayService.openOverlay(
      SignupComponent,
      true,
      this.elementRef,
      Number(REDDIT_CONSTANT.SIGN_UP_OVERLAY_WIDTH)
    );
  }

  initiateLogin() {
    if (this.loginForm.valid) {
      this.isloading = true;
      this.authService.authenticateUser(this.loginForm.value).subscribe({
        next: (response) => {
          this.handleSuccess(response);
        },
        error: (error) => {
          this.handleFailure(error);
        },
      });
    }
  }

  private handleFailure(error: any) {
    this.isloading = false;
    this.alertService.showAlert({
      type: 'error',
      message: error.error.message,
      componentId: 'log-in',
    });
  }

  private handleSuccess(response: object) {
    this.isloading = false;
    this.authWebService.storeSession(response);
    this.alertService.showAlert({
      type: 'success',
      message: 'User logged in successfully !',
      componentId: 'log-in',
    });
    // setting community data to load communities upon login
    this.communityService.fetchAllCommunities().subscribe((data) => {
      this.communityCdService.setCommunityData(data);
    });
    setTimeout(() => {
      this.overlayService.closeOverlay();
      this.catagoryCd.setCatagoryData(HOME_CATAGORY);
    }, 1000);
  }


  getLoginForm(): FormGroup {
    return this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
  }
}
