import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthWebService } from '../shared/services/auth-web.service';
import { environment } from 'src/environments/environment';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthWebService
  ) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let req = request;
    // if environment is production then just pass request url endpoint not the server info
    if(environment.production) {
      const reqMapping = req.url.split(':8080')[1];
      req = req.clone({url: reqMapping});
    }
    // Get the token from session storage
    const session:any = localStorage.getItem('user-session');
    let authReq = req;
    const s3Included = req.url.includes("reddit-clone-s3");
    if (session && !s3Included) {
      const token = JSON.parse(session).token;
      authReq = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${token}`)
        
      });
    }

    // Pass the cloned request instead of the original request to the next handler
    return next.handle(authReq).pipe(
        tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            // handle success response
            // You can perform actions based on the response such as logging, parsing data, etc.
          }
        },
        (error: HttpErrorResponse) => {
          // handle failure resposne
          console.error('Interceptor Error:', error);
          if(error.status === 403 || error.status === 401) {
            this.authService.clearSessionAndLogin();
          }
          // You can also throw an error or handle it based on your application's needs
        }
      )
    );
  }
}