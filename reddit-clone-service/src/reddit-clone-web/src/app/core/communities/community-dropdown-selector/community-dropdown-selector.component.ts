import { Component, ElementRef, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { CommunityResponse } from 'generated/angular-client';
import { map, Observable, Subject, takeUntil } from 'rxjs';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';

@Component({
  selector: 'app-community-dropdown-selector',
  templateUrl: './community-dropdown-selector.component.html',
  styleUrls: ['./community-dropdown-selector.component.scss']
})
export class CommunityDropdownSelectorComponent implements OnInit {
  @Output() emitSelectedCommunity: EventEmitter<CommunityResponse> = new EventEmitter();
  communityData$! : Observable<CommunityResponse[]>;
  filteredCommunities$! : Observable<CommunityResponse[]>;
  userName$!: Observable<any>;
  selectedCommunity: CommunityResponse | null = null;
  searchText!: string;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private el: ElementRef,
    private communityChangeDetecterService: CommunityChangeDetecterService,
    private authWebService: AuthWebService
  ) { }

  isOpen = false;

  ngOnInit(): void {
    this.communityData$ = this.filteredCommunities$ = this.communityChangeDetecterService.communityData$;
    this.userName$ = this.authWebService.currentUser$;
    this.populateCommunity()
  }

  populateCommunity() {
    this.communityChangeDetecterService.communityId$
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe((communityId) => {
      console.log(communityId)
      this.communityData$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (data) => data.forEach(
          (community) => {
            console.log(community.id, communityId)
            community.id === Number(communityId)? this.selectCommunity(community) :'';
          }
        ));
    })
  }

  toggleDropdown(): void {
    this.isOpen = !this.isOpen;
  }

  filterCommunities(): void {
    console.log(this.searchText)
    this.filteredCommunities$ = this.communityData$.pipe(
      map((data) => data.filter(
        (community) => {
          return community.communityName?.toLowerCase().includes(this.searchText)
        }
      )));
  }

  selectCommunity(community: CommunityResponse): void {
    this.selectedCommunity = community;
    this.isOpen = false;
    this.emitSelectedCommunity.emit(community);
  }

    // close the containers when clicking outside
    @HostListener('document:click', ['$event'])
    clickout(event: MouseEvent) {
      if (this.isOpen && !this.el.nativeElement.contains(event.target)) {
        this.isOpen = false;
      }
    }

    ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
    }

}
