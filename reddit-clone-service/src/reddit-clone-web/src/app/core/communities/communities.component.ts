import { Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommunityControllerService, CommunityResponse } from 'generated/angular-client';
import { Observable, Subject, takeUntil } from 'rxjs';
import { CatagoryChangeDetecterService } from 'src/app/change-detecters/catagory-change-detector.service';
import { CommunityChangeDetecterService } from 'src/app/change-detecters/community-change-detecter.service';
import { RegisterCommunityComponent } from 'src/app/components/community/register-community/register-community.component';
import { OverlayService } from 'src/app/shared/services/overlay.service';

@Component({
  selector: 'app-communities',
  templateUrl: './communities.component.html',
  styleUrls: ['./communities.component.scss']
})
export class CommunitiesComponent implements OnInit {

  communityData$! : Observable<CommunityResponse[]>;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private overlayService: OverlayService,
    private elementRef: ElementRef,
    private communityControllerService: CommunityControllerService,
    private communityDataService: CommunityChangeDetecterService,
    private readonly catagoryCd: CatagoryChangeDetecterService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadCommunities();
    this.communityData$ = this.communityDataService.communityData$;
  }

  openSubreddit(community: CommunityResponse) {
    this.catagoryCd.setCatagoryData(undefined);
    this.router.navigate(['/community', community.id]);
  }

  createNewCommunity() {
    this.overlayService.openOverlay(RegisterCommunityComponent,true,this.elementRef,600);
  }

  loadCommunities(): void {
    this.communityControllerService.fetchAllCommunities().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(
      (communities => {
        this.communityDataService.setCommunityData(communities);
      })
    );
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
