
import { Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CommunityResponse, GlobalSearchControllerService, NonEnrichedSearchResponse } from 'generated/angular-client';
import { debounceTime, Subject, takeUntil } from 'rxjs';
import { GlobalSearchChangeDetectorService } from 'src/app/change-detecters/global-search-change-detector.service';

@Component({
  selector: 'app-toolbar-search',
  templateUrl: './toolbar-search.component.html',
  styleUrls: ['./toolbar-search.component.scss']
})
export class ToolbarSearchComponent implements OnInit, OnDestroy {

  @ViewChild('searchContainer', { static: true }) searchContainer!: ElementRef<HTMLDivElement>;

  private unsubscribe$ = new Subject<void>();
  nonEnrichedSearchResponse!: NonEnrichedSearchResponse;
  selectedValue!: CommunityResponse | null;
  isOverlayOpen = false;
  searchText: string = '';

  constructor(
      private renderer: Renderer2,
      private el: ElementRef,
      private globalSearchController: GlobalSearchControllerService,
      private globalSearchChangeDetectorService: GlobalSearchChangeDetectorService,
      private router: Router
    ) { }

  ngOnInit(): void { }

  openOverlay() {
    this.renderer.addClass(this.searchContainer.nativeElement, 'rounded-bl-none');
    this.renderer.addClass(this.searchContainer.nativeElement, 'rounded-br-none');
    this.isOverlayOpen = true;
  }

  closeOverlay() {
    this.renderer.removeClass(this.searchContainer.nativeElement, 'rounded-bl-none');
    this.renderer.removeClass(this.searchContainer.nativeElement, 'rounded-br-none');
    this.isOverlayOpen = false
  }

  // close the containers when clicking outside
  @HostListener('document:click', ['$event'])
  clickout(event: MouseEvent) {
      if (this.isOverlayOpen && !this.el.nativeElement.contains(event.target)) {
          this.closeOverlay();
      }
  }

  globalSearch(event: any) {
    const searchText: string = event.target.value;
    if (searchText.length > 2) {
      this.globalSearchController.getNonEnrichedSearchResults(searchText)
        .pipe(
          takeUntil(this.unsubscribe$),
          debounceTime(500)
        ).subscribe((data) => {
          this.nonEnrichedSearchResponse = data;
          this.globalSearchChangeDetectorService.setNonEnrichedGlobalSearchData(data);
        });
    }
  }

  removeSelection() {
    this.selectedValue = null;
  }

  emittedSelectedValue(event: CommunityResponse) {
    console.log('Event received:', event);
    this.selectedValue = event;
    this.closeOverlay();
    // reset input and navigate
    this.searchText = '';
    this.router.navigate(['/community', event.id]);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
