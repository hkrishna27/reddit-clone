
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CommunityResponse, NonEnrichedSearchResponse } from 'generated/angular-client';
import { Observable } from 'rxjs';
import { GlobalSearchChangeDetectorService } from 'src/app/change-detecters/global-search-change-detector.service';

@Component({
  selector: 'app-search-overlay',
  templateUrl: './search-overlay.component.html',
  styleUrls: ['./search-overlay.component.scss']
})
export class SearchOverlayComponent implements OnInit {
  nonEnrichedSearchResponse$!: Observable<NonEnrichedSearchResponse>;
  isGlobalSearchData = false;
  @Output() emitSelectedCommunity: EventEmitter<CommunityResponse> = new EventEmitter();

  constructor(
    private globalSearchChangeDetectorService: GlobalSearchChangeDetectorService
  ) {}


  ngOnInit(): void {
    this.nonEnrichedSearchResponse$ = this.globalSearchChangeDetectorService.nonEnrichedSearchResponse$;
    this.nonEnrichedSearchResponse$
    .subscribe(
      data => {
          this.isGlobalSearchData =  data.communities?.length! > 0;
      });
  }

  selectCommunity(community: CommunityResponse) {
    console.log(community)
    this.emitSelectedCommunity.emit(community);
  }
}
