import { Component, ElementRef, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProfileComponent } from 'src/app/components/profile/profile.component';
import { LogInComponent } from 'src/app/authentication/security/log-in/log-in.component';
import { AuthWebService } from 'src/app/shared/services/auth-web.service';
import { OverlayService } from 'src/app/shared/services/overlay.service';

@Component({
  selector: 'app-right-pane-toolbar',
  templateUrl: './right-pane-toolbar.component.html',
  styleUrls: ['./right-pane-toolbar.component.scss']
})
export class RightPaneToolbarComponent implements OnInit {

  userName$!: Observable<any>;
  isAuthenticated$!: Observable<boolean>;
  constructor(private overlayService: OverlayService,
    private elementRef: ElementRef,
    private authWebService: AuthWebService) { }

  ngOnInit(): void {
    this.authWebService.ngOnInit();
    this.isAuthenticated$ = this.authWebService.isAuthenticated$;
    this.userName$ = this.authWebService.currentUser$;
    this.authWebService.initiateLogin$.subscribe(
      (_isLoginRequired:boolean) => {
        if (_isLoginRequired) {
          setTimeout(()=> {
            // close if any open overlay
            this.overlayService.closeOverlay()
            this.initiateLogIn();
          },500);
          this.initiateLogIn();
        }
      }
    );
  }

  initiateLogIn() {
    this.overlayService.openOverlay(LogInComponent,true,this.elementRef,400);
  }
  openProfileOverlay() {
    this.overlayService.openOverlay(ProfileComponent,false,this.elementRef,250);
  }
}
