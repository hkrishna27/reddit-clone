import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/shared/models/sidebar-menu';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
	CATAGORY: string = 'catagory';
	menuItems: MenuItem[] = [
		{
			link: '/home',
			name: 'Home',
			icon: 'home',
			isExpandable: false,
			children: [],
		},
		{
			link: '/trending',
			name: 'Popular',
			icon: 'trending_up',
			isExpandable: false,
			children: [],
		},
		{
			link: '/gaming',
			name: 'Gaming',
			icon: 'gamepad',
			isExpandable: true,
			children: [
				{
					link: '/call-of-duty',
					name: 'Call of Duty: Warzone',
					icon: '',
					isExpandable: false,
					children: [],
				},
				{
					link: '/minecraft',
					name: 'Minecraft',
					icon: '',
					isExpandable: false,
					children: [],
				},
				{
					link: '/path-of-exile',
					name: 'Path of Exile',
					icon: '',
					isExpandable: false,
					children: [],
				},
				{
					link: '/gta-5',
					name: 'Gta 5',
					icon: '',
					isExpandable: false,
					children: [],
				},
			],
		},
		{
			link: '/sports',
			name: 'Sports',
			icon: 'directions_bike',
			isExpandable: true,
			children: [
				{
					link: '/football',
					name: 'Football',
					icon: '',
					isExpandable: false,
					children: [],
				},
				{
					link: '/cricket',
					name: 'Cricket',
					icon: '',
					isExpandable: false,
					children: [],
				},
				{
					link: '/tennis',
					name: 'Tennis',
					icon: '',
					isExpandable: false,
					children: [],
				},
			],
		},
		{
			link: `/${this.CATAGORY}/careers`,
			name: 'Careers',
			icon: 'local_library',
			isExpandable: false,
			children: [],
		},
		{
			link: `/${this.CATAGORY}/coding`,
			name: 'Coding',
			icon: 'developer_board',
			isExpandable: false,
			children: [],
		},
	];
	constructor() { }

	ngOnInit(): void { }
}
