import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LayoutComponent } from './layout/layout.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { ToolbarSearchComponent } from './sidebar/toolbar-search/toolbar-search.component';
import { SearchOverlayComponent } from './sidebar/toolbar-search/search-overlay/search-overlay.component';
import { RightPaneToolbarComponent } from './sidebar/right-pane-toolbar/right-pane-toolbar.component';
import { CommunitiesComponent } from './communities/communities.component';
import { CommunityDropdownSelectorComponent } from './communities/community-dropdown-selector/community-dropdown-selector.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SidebarComponent,
    LayoutComponent,
    ToolbarSearchComponent,
    SearchOverlayComponent,
    RightPaneToolbarComponent,
    CommunitiesComponent,
    CommunityDropdownSelectorComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule
    ],
  exports: [SidebarComponent,LayoutComponent,CommunityDropdownSelectorComponent],
})
export class CoreModule { }
