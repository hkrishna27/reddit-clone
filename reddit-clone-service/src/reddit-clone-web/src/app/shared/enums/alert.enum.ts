export enum Alerts {
    required= 'This field is required',
    minlength= 'The entered value is too short',
    maxlength= 'The entered value is too long',
    pattern= 'Password must contain atleast one digit, one uppercase and one special character.',
    passwordMismatch= 'Password do not match',
    email= 'Please enter a valid email address', 
    communityAlreayExist = "This community already exists"
}