export enum REDDIT_CONSTANT {
    // auth constants
    FORGOT_PASSWORD= "Forgot Password ?",
    SIGN_UP = "Sign Up",
    NEW_TO_REDDIT = "New to reddit ?",
    LOG_IN = "Log In",
    LOG_IN_OVERLAY_WIDTH = "400",
    SIGN_UP_OVERLAY_WIDTH = "400",
    // accessibility constant
    PUBLIC = "PUBLIC",
    PRIVATE = "PRIVATE",
    RESTRICTED = "RESTRICTED",
    // community constant
    LOADING_TOPICS = "Loading Topics",
    SUBREDDIT_PREFIX = "r/",
    // s3
    COMMUNITY_S3 = "community",
    VIDEO_S3= "videos",
    UPLOAD_SUCCESS = "Upload Success",
    UPLOAD_FAILED = "Upload Failed",
    SIGNED_URL_FAILED = "Not able to get signed Url",
    ERROR = "Something went wrong",
    S3_UPLOAD_FAILED = "S3 upload failed",
    MULTIPLE_VIDEOS_UPLOAD = "Muliple videos in gallary",
    //pageable
    SORT_BY_ID = "id",
    DIRECTION_ASC = "ASC",
    DIRECTION_DESC = "DESC",
    // posts
    UPWARD = "UPWARD",
    DOWNWORD = "DOWNWARD",
    BOTH_IMAGE_AND_VIDEO = "Videos and images can't be uploaded together",
    MAX_SIZE_EXCEEDED = "You can only upload 5 images",
    MULTIPLE_VIDEO = "More than one video can not be uploaded"
}