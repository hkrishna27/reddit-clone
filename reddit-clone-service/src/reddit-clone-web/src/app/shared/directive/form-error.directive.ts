import { Directive, Input, OnInit, ElementRef } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { FormErrorService } from '../services/form-error.service';

@Directive({
  selector: '[appFormError]'
})
export class FormErrorDirective implements OnInit {
  @Input() control!: AbstractControl;
  @Input() form!: FormGroup;

  constructor(private el: ElementRef, private formErrorService: FormErrorService) {}

  ngOnInit(): void {
    const control: AbstractControl | null = this.control;
    if (control) {
      control.statusChanges.subscribe(() => {
        this.showError(control);
      });
    }
  }

  private showError(control: AbstractControl): void {
    const errorMessage = this.formErrorService.getErrorMessage(control);
    this.el.nativeElement.innerHTML = errorMessage;
  }
}
