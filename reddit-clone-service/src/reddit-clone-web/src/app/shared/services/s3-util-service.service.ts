import { Injectable } from '@angular/core';
import { S3ControllerService, SignedUrlRequestS3 } from 'generated/angular-client';
import { forkJoin, map, Observable, switchMap } from 'rxjs';
import { REDDIT_CONSTANT } from '../constants/shared.constant';

@Injectable({
  providedIn: 'root'
})
export class S3UtilServiceService {
  progress: { [key: string]: number } = {};
  urlMap: { [key: string]: string | undefined } = {};

  constructor(
    private s3Backend: S3ControllerService
  ) { }

  signedUrlRequestS3(file:File, s3BucketFolder: string) {
    return {
      fileName: file.name, s3BucketFolder, httpMethod: SignedUrlRequestS3.HttpMethodEnum.Put
    }
  }

  splitImageUrlFromPreSignedUrl(presignedUrl:string):string {
    return presignedUrl.split('?')[0];
  }
  

  // uploadFilesToS3(files: File[], s3BucketFolder: REDDIT_CONSTANT) {
  //   for(let i=0;i<files.length;i++) {
  //     if(files[i]) {
  //       {
  //         const request: SignedUrlRequestS3 = { fileName: files[i].name, s3BucketFolder, httpMethod: SignedUrlRequestS3.HttpMethodEnum.Put };
  //         // call api to get pre-signed url
  //         console.log(request);
  //         this.s3Backend.getSignedUrlS3(request).pipe(
  //           switchMap(s3Res => {
  //             console.log(s3Res)
  //             // if pre signed url is threre then upload image
  //             if (s3Res?.signedUrl) {
  //               // call s3 client api to uplaod video
  //               return this.uploadFile(s3Res.signedUrl,files[i],(progress) => {
  //                 this.progress = progress;
  //               }).pipe(
  //                 map(_data => {
  //                   // if successful write s3Image url and return observable
  //                   return { success: true, message: REDDIT_CONSTANT.UPLOAD_SUCCESS , s3ImageUrl: s3Res.signedUrl };
  //                 }),
  //                 // if failed during upload then send error response as observable
  //                 catchError((error: HttpErrorResponse) => {
  //                   //this.showAlertsIfErrorPopsUp();
  //                   return of({ success: false, message: REDDIT_CONSTANT.UPLOAD_FAILED, s3ImageUrl: null });
  //                 })
  //               );
  //             }
  //             // if pre-signed url is not there send a observable with the error
  //             return of({ success: false, message: REDDIT_CONSTANT.SIGNED_URL_FAILED, s3ImageUrl: null });
  //           }),
  //           // if pre-signed api fails then return error response as observable
  //           catchError(error => {
  //             //this.showAlertsIfErrorPopsUp();
  //             return of({ success: false, message: REDDIT_CONSTANT.S3_UPLOAD_FAILED, s3ImageUrl: null });
  //           })
  //         ).subscribe();
  //     }
  //   }
   
  // }

  // }

  uploadFilesToS3WithSignedUrls(files: File[], s3BucketFolder: REDDIT_CONSTANT): Observable<any[]> {
    const requests: SignedUrlRequestS3[] = files.map(file => this.signedUrlRequestS3(file, s3BucketFolder));

    return this.s3Backend.getSignedUrlsS3(requests).pipe(
      map(s3Res => {
        s3Res.forEach(data => {
          this.urlMap[data.fileName!] = data.signedUrl;
        });
        return this.urlMap;
      }),
      switchMap(urls => this.uploadFiles(files, urls, (fileName, progress) => {
        this.progress[fileName] = progress;
      }))
    );
  }

  getProgressOfFile(fileName: string) {
    return this.progress[fileName] | 0;
  }

  getUrlMapsOfSignedUrl() {
    return this.urlMap;
  } 


  uploadFiles(files: File[], urls: { [key: string]: string | undefined }, progressCallback: (fileName: string, progress: number) => void): Observable<any[]> {
    const uploadObservables = files.map(file => {
      return this.uploadFile(urls[file.name], file, (progress) => {
        progressCallback(file.name, progress);
      });
    });
    return forkJoin(uploadObservables);
  }

  uploadFile(url: string | undefined, file: File, progressCallback: (progress: number) => void): Observable<any> {
    return new Observable(observer => {
      const xhr = new XMLHttpRequest();
      xhr.open('PUT', url!, true);
      xhr.setRequestHeader('Content-Type', file.type);

      xhr.upload.onprogress = (event) => {
        if (event.lengthComputable) {
          const progress = Math.round((event.loaded / event.total) * 100);
          progressCallback(progress);
        }
      };

      xhr.onload = () => {
        if (xhr.status === 200) {
          observer.next(xhr.response);
          observer.complete();
        } else {
          observer.error(xhr.response);
        }
      };

      xhr.onerror = () => observer.error(xhr.response);

      xhr.send(file);
    });
  }
}
