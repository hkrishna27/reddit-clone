import { Injectable } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import { Alerts } from "../enums/alert.enum";

@Injectable({
    providedIn: 'root'
})
export class FormErrorService {
    private errorMessages: { [key: string]: string } = Alerts;
    
    getErrorMessage(control: AbstractControl): string {
        if (control.errors) {
          for (const errorKey in control.errors) {
            if (this.errorMessages[errorKey]) {
              return this.errorMessages[errorKey];
            }
          }
        }
        return '';
      }
}