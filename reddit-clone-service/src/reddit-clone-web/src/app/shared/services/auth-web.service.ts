import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserResponse } from 'generated/angular-client';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthWebService implements OnInit {
  private isAuthenticatedSubject$ = new BehaviorSubject<boolean>(false);
  isAuthenticated$: Observable<boolean> = this.isAuthenticatedSubject$.asObservable();
  private currentUserSubject$: BehaviorSubject<any> = new BehaviorSubject<any>({});
  currentUser$: Observable<any>  = this.currentUserSubject$.asObservable();
  private initiateLoginSubject$ = new BehaviorSubject<boolean>(false);
  initiateLogin$: Observable<boolean> = this.initiateLoginSubject$.asObservable();

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    const userSession = JSON.parse(localStorage.getItem('user-session')!);
    if (userSession) {
      this.currentUserSubject$.next(userSession);
      this.isAuthenticatedSubject$.next(true);
    }
  }

  storeSession(response:any) {
    if(response && response.token) {
      localStorage.setItem('user-session',JSON.stringify(response));
      this.currentUserSubject$.next(response);
      this.isAuthenticatedSubject$.next(true);
      this.currentUser$ = this.currentUserSubject$.asObservable();
      this.isAuthenticated$ = this.isAuthenticatedSubject$.asObservable();
      
    }
  }

  public get currentUserObservable(): Observable<string> {;
    return this.currentUserSubject$;
  }

  getCurrentUserValue(): any {
    return this.currentUserSubject$.value;
  }

  public get isAutheticated():Observable<boolean> {
    return this.isAuthenticated$;
  }

  clearSession() {
    localStorage.removeItem('user-session');
    this.currentUserSubject$.next({});
    this.isAuthenticatedSubject$.next(false);
  }

  /**
   * clear session from storage and open login page
   */
  clearSessionAndLogin() {
    this.clearSession();
    this.initiateLoginSubject$.next(true);
    this.router.navigate(['']);
  }

  /**
   * 
   * @returns current user UserResponse mapped model
   */
  getUserResponse(): UserResponse {
    const user = this.getCurrentUserValue();
    return {
      userId: user?.id,
      userEmail: user?.email,
      userName: user?.username,
      userRoles: user?.roles
    }
  }
}
