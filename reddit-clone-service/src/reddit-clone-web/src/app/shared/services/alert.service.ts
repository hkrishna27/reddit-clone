import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface Alert {
  type: 'success' | 'error' | 'info' | 'warning';
  message: string;
  componentId: string;
}

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private alert$: BehaviorSubject<Alert | null> = new BehaviorSubject<Alert | null>(null);
  constructor() { }
  getAlert(): Observable<Alert | null> {
    return this.alert$.asObservable();
  }

  showAlert(alert: Alert): void {
    this.alert$.next(alert);
  }

  clearAlert(componentId: string): void {
    const currentAlert = this.alert$.value;
    if (currentAlert && currentAlert.componentId === componentId) {
      this.alert$.next(null);
    }
  }
}
