import { Overlay, OverlayRef, PositionStrategy } from '@angular/cdk/overlay';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';
import { ElementRef, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class OverlayService {
  private overlayRef: OverlayRef | null = null;
  private overlayStateSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  
  constructor(private overlay: Overlay) { }
  
  openOverlay(component:ComponentType<any>,openInCenter:boolean,elementRef:ElementRef,searchContainerWidth:number) {
    if (!this.overlayRef) {
      let positionStrategy:PositionStrategy;
      if(!openInCenter) {
      positionStrategy = this.overlay.position()
        .flexibleConnectedTo(elementRef)
        .withPositions([{
          originX: 'start',
          originY: 'bottom',
          overlayX: 'start',
          overlayY: 'top',
          offsetY:0
        }]);
      } else {
        positionStrategy = this.overlay.position().global().centerHorizontally().centerVertically();
      }
      // this.overlayRef = this.overlay.create({ positionStrategy, width: searchContainerWidth });
      this.overlayRef = this.overlay.create({
        hasBackdrop: true,
        positionStrategy: positionStrategy,
        width: searchContainerWidth
      });
      const portal = new ComponentPortal(component);
      this.overlayRef.attach(portal);
      this.overlayRef.backdropClick().subscribe(() => this.closeOverlay());
    }
    this.overlayStateSubject.next(true);
  }

  closeOverlay() {
    if (this.overlayRef) {
      this.overlayRef.detach();
      this.overlayRef = null;
      this.overlayStateSubject.next(false);
    }
  }

  getOverlayState() {
    return this.overlayStateSubject.asObservable();
  }

}
