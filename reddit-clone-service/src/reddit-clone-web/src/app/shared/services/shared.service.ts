import { Injectable } from '@angular/core';
import { PageableRequest } from 'generated/angular-client';
import { REDDIT_CONSTANT } from '../constants/shared.constant';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() { }

  getPageableRequest(): PageableRequest {
    return { page: 0, size: 100, sortBy: REDDIT_CONSTANT.SORT_BY_ID, direction: REDDIT_CONSTANT.DIRECTION_ASC };
  }

  timeSinceCalculator(timestamp: string): string {
    const date = new Date(timestamp);
    const now = new Date();
    const secondsPast = (now.getTime() - new Date(date).getTime()) / 1000;
  
    if (secondsPast < 60) {
      return `${Math.floor(secondsPast)} seconds ago`;
    }
    if (secondsPast < 3600) {
      return `${Math.floor(secondsPast / 60)} minutes ago`;
    }
    if (secondsPast < 86400) {
      return `${Math.floor(secondsPast / 3600)} hours ago`;
    }
    if (secondsPast < 2592000) {
      return `${Math.floor(secondsPast / 86400)} days ago`;
    }
    if (secondsPast < 31536000) {
      return `${Math.floor(secondsPast / 2592000)} months ago`;
    }
    return `${Math.floor(secondsPast / 31536000)} years ago`;
  }

  stripHtml(html: string): string {
    if (!html) return '';
  
    // Create a new div element
    const div = document.createElement('div');
  
    // Set the HTML content with the provided string
    div.innerHTML = html;
  
    // Retrieve the text property of the element, which strips out the HTML tags
    return div.textContent || div.innerText || '';
  }
  
}
