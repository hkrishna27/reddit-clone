import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthWebService } from '../services/auth-web.service';
import { OverlayService } from '../services/overlay.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authWebService:AuthWebService,
    private overlayService: OverlayService,
    private router: Router
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): boolean {
    if(this.authWebService.isAuthenticated$) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
  
}
