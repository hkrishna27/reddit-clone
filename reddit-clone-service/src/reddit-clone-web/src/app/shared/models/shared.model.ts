export interface AccessibityOptions {
    icon: string,
    name: string,
    description: string,
    checked: boolean
}