export interface MenuItem {
    link: string,
    name: string,
    icon: string,
    isExpandable: boolean,
    children: MenuItem[]
}