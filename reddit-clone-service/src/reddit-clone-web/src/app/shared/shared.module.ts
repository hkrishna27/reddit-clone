import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormErrorDirective } from './directive/form-error.directive';
import { AlertComponent } from './components/alert/alert.component';
import { MaterialModule } from '../material/material.module';
import { TruncatePipe } from './pipe/truncate.pipe';
import { FileUrlPipe } from './pipe/url.pipe';


@NgModule({
  declarations: [FormErrorDirective, AlertComponent,TruncatePipe,FileUrlPipe],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    FormErrorDirective,
    AlertComponent,
    TruncatePipe,
    FileUrlPipe
  ],
  providers: []
})
export class SharedModule { }
