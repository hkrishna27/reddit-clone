import { Component, Input, OnInit } from '@angular/core';
import { Alert, AlertService } from '../../services/alert.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  @Input() component! : string;
  alerts: Alert[] = [];
  alertSubsription$!: Subscription;
    // Define the mapping between alert types and icon names
    iconMap: { [key: string]: string } = {
      'success': 'done',
      'error': 'error',
      'info': 'info',
      'warning': 'warning'
    };

  constructor(private alertService: AlertService) { }

  ngOnInit(): void {
    this.alertSubsription$ = this.alertService.getAlert().subscribe(alert => {
      if(alert) {
        this.alerts.push(alert);
        setTimeout(()=> {
          this.clearAlert(alert.componentId);
          this.alertService.clearAlert(alert.componentId);
        },1000);
      }
    })
  }

  clearAlert(componentId: string): void {
    this.alerts = this.alerts.filter(alert => alert.componentId !== componentId);
  }

  ngOnDestroy(): void {
    this.alertSubsription$.unsubscribe();
  }

}
