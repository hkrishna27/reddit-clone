import { AbstractControl, ValidationErrors } from "@angular/forms";

export function setControlError(control: AbstractControl | null, error: ValidationErrors | null){
    if(!!control && !!error){
        control.setErrors({...control.errors, ...error});
    }
}