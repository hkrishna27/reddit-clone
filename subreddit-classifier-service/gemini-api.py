from flask import Flask, request
from constants import GOOGLE_API_KEY,PREFIX_T0_REQUEST
import ast

import google.generativeai as genai


app = Flask(__name__)

@app.route("/", methods=["POST"])
def index():
    prompt = request.json['prompt']
    genai.configure(api_key = GOOGLE_API_KEY)
    model = genai.GenerativeModel('gemini-pro')
    response = model.generate_content(PREFIX_T0_REQUEST + prompt)
    print(response)
    listOfSubReddits = response.text.split(',');
    return listOfSubReddits

if __name__ == '__main__':
  app.run()