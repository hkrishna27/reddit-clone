# Steps to run Gemini Classifier

1. `Python -m venv venv`
2. `source venv/scripts/activate`
3. `python.exe -m pip install --upgrade pip`
4. `pip install Flask`
5. `pip install -q -U google-generativeai`